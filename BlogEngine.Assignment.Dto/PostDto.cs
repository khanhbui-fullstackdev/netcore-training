﻿using BlogEngine.Assignment.Dto.BaseDto;

namespace BlogEngine.Assignment.Dto
{
    public class PostDto : AuditDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }

        public string Image { get; set; }

        public string Summary { get; set; }

        public string Content { get; set; }

        public string Quote { get; set; }

        public int CategoryID { get; set; }

        public int CountLikes { get; set; }

        public int CountComments { get; set; }

        public string CategoryName { get; set; }
    }
}
