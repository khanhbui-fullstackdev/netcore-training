﻿using System;

namespace BlogEngine.Assignment.Dto.BaseDto
{
    public abstract class AuditDto
    {
        public DateTime? CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public bool Status { get; set; }
    }
}
