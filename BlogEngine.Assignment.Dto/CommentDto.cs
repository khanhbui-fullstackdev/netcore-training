﻿using System;
using System.Collections.Generic;

namespace BlogEngine.Assignment.Dto
{
    public class CommentDto
    {
        public Guid CommentId { get; set; }

        public int Id { set; get; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Content { get; set; }

        public string Email { get; set; }

        public int PostID { get; set; }

        public virtual PostDto Post { get; set; }

        public int CountReplyComments { get; set; }

        public int CountLikes { get; set; }

        public virtual IEnumerable<ReplyCommentDto> ReplyComments { get; set; }
    }
}
