﻿using BlogEngine.Assignment.Dto.BaseDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogEngine.Assignment.Dto
{
    public class CategoryDto : AuditDto
    {
        public int ID { get; set; }

        public string Name { get; set; }
        
        public string Slug { get; set; }
 
        public string Image { get; set; }

        public string Summary { get; set; }

        public virtual ICollection<SubCategoryDto> SubCategories { get; set; }
    }
}
