﻿using BlogEngine.Assignment.Dto.BaseDto;

namespace BlogEngine.Assignment.Dto
{
    public class SubCategoryDto : AuditDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }

        public string Summary { get; set; }

        public int CategoryID { get; set; }
    }
}
