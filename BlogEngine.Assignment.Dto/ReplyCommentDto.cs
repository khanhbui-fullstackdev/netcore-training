﻿using System;

namespace BlogEngine.Assignment.Dto
{
    public class ReplyCommentDto
    {
        public Guid ReplyId { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Content { get; set; }

        public int CountLikes { get; set; }

        public Guid CommentId { get; set; }

        public virtual CommentDto Comment { get; set; }
    }
}
