﻿namespace BlogEngine.Assignment.Common.Enums
{
    public enum LikeType
    {
        Comment = 0,
        Reply = 1
    }
}
