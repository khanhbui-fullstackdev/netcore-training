﻿namespace BlogEngine.Assignment.Common.Enums
{
    public enum SortingType
    {
        Default = 0, // None sorting

        MostFavorite = 1, // Sorting by counting like post
        LessFavorite = 2,

        Newest = 3, // Sorting by created date desc
        Oldest = 4, // Sorting by created date asc

        MostComment = 5, // Sorting by counting comments desc
        LessComment = 6,

        NameAlphabetAsc = 7,
        NameAlphabetDesc = 8
    }
}
