﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogEngine.Assignment.Common.Constants
{
    public static class ViewUrl
    {        
        public const string _Category = "~/Views/Shared/_Category.cshtml";
        public const string _Footer = "~/Views/Shared/_Footer.cshtml";
        public const string _Header = "~/Views/Shared/_Header.cshtml";
        public const string _LastestComment = "~/Views/Shared/_LastestComment.cshtml";
        public const string _NewsLettter = "~/Views/Shared/_NewsLetter.cshtml";
        public const string _Pagination = "~/Views/Shared/_Pagination.cshtml";

        public const string _CommentArea = "~/Views/Post/CommentPanel/_CommentArea.cshtml";
        public const string _CommentText = "~/Views/Post/CommentPanel/_CommentText.cshtml";

        public const string _ReplyCommentArea = "~/Views/Comment/ReplyCommentPanel/_ReplyCommentArea.cshtml";
        public const string _ReplyCommentText = "~/Views/Comment/ReplyCommentPanel/_ReplyCommentText.cshtml";

        public const string _SignIn = "~/Views/Account/Panel/_SignIn.cshtml";

        public const string _BreadCrumb = "~/Views/Shared/Components/BreadCrumb/_BreadCrumb.cshtml";
        public const string _SortingCriteria = "~/Views/Shared/Components/SortingCriteria/_SortingCriteria.cshtml";

        public const string HomeIndex = "~/Views/Home/Index.cshtml";

        public const string PostIndex = "~/Views/Post/Index.cshtml";
        public const string PostsByCategory = "~/Views/Post/PostsByCategory.cshtml";
        public const string PostsByKeyword = "~/Views/Post/PostsByKeyword.cshtml";
        public const string PostsBySubCategory = "~/Views/Post/PostsBySubCategory.cshtml";
    }
}
