﻿using BlogEngine.Assignment.Web.BaseViewModels;
using System.Collections.Generic;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class CategoryViewModel : AuditViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }
       
        public string Image { get; set; }

        public string Summary { get; set; }

        public virtual ICollection<SubCategoryViewModel> SubCategories { get; set; }
    }
}
