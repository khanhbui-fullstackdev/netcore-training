﻿using BlogEngine.Assignment.Web.BaseViewModels;
using System;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class ReplyCommentViewModel : AuditViewModel
    {
        public Guid ReplyId { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Content { get; set; }

        public int CountReplyComments { get; set; } = 0;

        public int CountLikes { get; set; } = 0;

        public virtual CommentViewModel Comment { get; set; }

        public Guid CommentId { get; set; }
    }
}
