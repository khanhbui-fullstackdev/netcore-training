﻿using BlogEngine.Assignment.Web.BaseViewModels;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class SubCategoryViewModel : AuditViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }

        public int CategoryID { get; set; }
    }
}

