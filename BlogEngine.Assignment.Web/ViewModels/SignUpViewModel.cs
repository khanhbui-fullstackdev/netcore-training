﻿using BlogEngine.Assignment.Common.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class SignUpViewModel
    {
        public string UserId { get; set; }

        [
            Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
            MinLength(6, ErrorMessage = ErrorMessage.MinLengthField),
            MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthField)]
        public string UserName { get; set; }

        [
            Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
            MinLength(2, ErrorMessage = ErrorMessage.MinLengthField),
            MaxLength(255, ErrorMessage = ErrorMessage.MaxLengthField)]
        public string FirstName { get; set; }

        [
            Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
            MinLength(2, ErrorMessage = ErrorMessage.MinLengthField),
            MaxLength(255, ErrorMessage = ErrorMessage.MaxLengthField)]
        public string LastName { get; set; }

        [
           EmailAddress(ErrorMessage = ErrorMessage.InvalidEmail),
           Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredEmail),
           MinLength(12, ErrorMessage = ErrorMessage.MinLengthEmail),
           MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthEmail)]
        public string Email { get; set; }

        [Required(ErrorMessage = ErrorMessage.RequiredBirthday)]
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }

        [MinLength(10, ErrorMessage = ErrorMessage.MinLengthPassword)]
        [MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthPassword)]
        public string Password { get; set; }

        [MinLength(10, ErrorMessage = ErrorMessage.MinLengthPassword)]
        [MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthPassword)]
        [Compare("Password", ErrorMessage = "Your password doesn't match")]
        public string ConfirmPassword { get; set; }       
    }
}