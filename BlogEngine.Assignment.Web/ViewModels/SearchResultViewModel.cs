﻿using System.Collections.Generic;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class SearchResultViewModel
    {
        public IEnumerable<PostViewModel> Posts { get; set; }
        public IEnumerable<CategoryViewModel> Categories { get; set; }
    }
}