﻿using BlogEngine.Assignment.Common.Enums;
using System;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class LikeCommentViewModel
    {
        public int Id { get; set; }

        public Guid? CommentId { get; set; }

        public Guid? ReplyCommentId { get; set; }

        public string UserName { get; set; }

        public int CountLikeComments { get; set; }

        public int CountLikeReplyComments { get; set; }

        public LikeType LikeType { get; set; }
    }
}
