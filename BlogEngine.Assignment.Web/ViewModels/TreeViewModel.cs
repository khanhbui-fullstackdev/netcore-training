﻿using BlogEngine.Assignment.Web.BaseViewModels;
using System.Collections.Generic;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class TreeViewModel : TreeViewBase
    {
        // Cheating data structure for using bootstrap-treeview
        // More reference: https://github.com/jonmiles/bootstrap-treeview/pull/116/files/7754cd7fc4f328b0fda25255ced1745cc8be541b?diff=split&short_path=04c6e90
        public int _parentid { get; set; }
        public string _slug { get; set; }
        public List<NodeViewModel> Nodes { get; set; }

        public TreeViewModel TreeViewMappingCategory(CategoryViewModel categoryViewModel)
        {
            TreeViewModel treeViewModel = new TreeViewModel();
            treeViewModel.Text = categoryViewModel.Name;
            treeViewModel._parentid = categoryViewModel.ID;
            treeViewModel._slug = categoryViewModel.Slug;

            if (categoryViewModel.SubCategories.Count != 0)
            {
                foreach (var subcategory in categoryViewModel.SubCategories)
                {
                    NodeViewModel nodeViewModel = new NodeViewModel();
                    nodeViewModel._nodeid = subcategory.ID;
                    nodeViewModel.Text = subcategory.Name;
                    nodeViewModel._slug = subcategory.Slug;
                    treeViewModel.Nodes.Add(nodeViewModel);
                }
            }
            return treeViewModel;
        }

        public List<TreeViewModel> TreeViewMappingCategory(IEnumerable<CategoryViewModel> categoriesViewModel)
        {
            List<TreeViewModel> treeViewModels = new List<TreeViewModel>();

            foreach (var categoryViewModel in categoriesViewModel)
            {
                TreeViewModel treeViewModel = new TreeViewModel
                {
                    _parentid = categoryViewModel.ID,
                    Text = categoryViewModel.Name,
                    _slug = categoryViewModel.Slug,
                    Href = "/category/" + categoryViewModel.Slug + "/" + categoryViewModel.ID, //category/{slug}/{id}
                    Tags = new List<int>()
                };
                if (categoryViewModel.SubCategories.Count != 0)
                {
                    treeViewModel.Tags.Add(categoryViewModel.SubCategories.Count);
                    treeViewModel.Nodes = new List<NodeViewModel>();
                    foreach (var subcategory in categoryViewModel.SubCategories)
                    {
                        NodeViewModel nodeViewModel = new NodeViewModel();
                        nodeViewModel._nodeid = subcategory.ID;
                        nodeViewModel.Text = subcategory.Name;
                        nodeViewModel._slug = subcategory.Slug;
                        nodeViewModel.Href = "/subcategory/" + subcategory.Slug + "/" + subcategory.ID;
                        treeViewModel.Nodes.Add(nodeViewModel);
                    }
                }
                else
                {
                    treeViewModel.Tags.Add(0);
                }
                treeViewModels.Add(treeViewModel);
            }
            return treeViewModels;
        }
    }
}