﻿using System;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class LikePostViewModel
    {
        public Guid LikePostId { get; set; }

        public int Id { get; set; }

        public string UserName { get; set; }

        public int PostId { get; set; }

        public int CountLikesPost { get; set; }
    }
}
