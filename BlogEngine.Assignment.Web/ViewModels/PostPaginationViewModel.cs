﻿using BlogEngine.Assignment.Web.Infrastructure.Cores;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class PaginationSetViewModel
    {
        public PaginationSet<PostViewModel> PostPaginationSet { get; set; }
    }
}