﻿using BlogEngine.Assignment.Common.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class AccountInfoViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField)]
        [MinLength(2, ErrorMessage = ErrorMessage.MinLengthField)]
        [MaxLength(255, ErrorMessage = ErrorMessage.MaxLengthField)]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField)]
        [MinLength(6, ErrorMessage = ErrorMessage.MinLengthUserName)]
        [MaxLength(30, ErrorMessage = ErrorMessage.MaxLengthUserName)]
        public string UserName { set; get; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField)]
        [MinLength(2, ErrorMessage = ErrorMessage.MinLengthField)]
        [MaxLength(255, ErrorMessage = ErrorMessage.MaxLengthField)]
        public string LastName { get; set; }

        public string FullName { get; set; }

        [MinLength(10, ErrorMessage = ErrorMessage.MinLengthPassword)]
        [MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthPassword)]
        public string OldPassword { get; set; }

        [MinLength(10, ErrorMessage = ErrorMessage.MinLengthPassword)]
        [MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthPassword)]
        public string NewPassword { get; set; }

        [MinLength(10, ErrorMessage = ErrorMessage.MinLengthPassword)]
        [MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthPassword)]
        [Compare("NewPassword", ErrorMessage = "Your password doesn't match")]
        public string ConfirmPassword { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField)]
        [MinLength(12, ErrorMessage = ErrorMessage.MinLengthEmail)]
        [MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthEmail)]
        [EmailAddress(ErrorMessage = ErrorMessage.InvalidEmail)]
        public string Email { get; set; }
        
        
        [Required(ErrorMessage = ErrorMessage.RequiredBirthday)]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
    }
}