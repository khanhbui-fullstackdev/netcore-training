﻿using BlogEngine.Assignment.Web.BaseViewModels;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class NodeViewModel : TreeViewBase
    {
        public int _nodeid { get; set; }

        public string _slug { get; set; }
    }
}