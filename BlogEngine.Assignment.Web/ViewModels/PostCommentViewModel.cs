﻿using System.Collections.Generic;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class PostCommentViewModel
    {
        public PostViewModel Post { get; set; }
        public IEnumerable<CommentViewModel> Comments { get; set; }
    }
}