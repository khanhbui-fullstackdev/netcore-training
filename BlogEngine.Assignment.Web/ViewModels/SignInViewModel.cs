﻿using BlogEngine.Assignment.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class SignInViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField)]
        [StringLength(50)]
        [MinLength(6, ErrorMessage = ErrorMessage.MinLengthUserName)]
        [MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthUserName)]
        public string UsernameOrEmail { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField)]
        [StringLength(50)]
        [MinLength(10, ErrorMessage = ErrorMessage.MinLengthPassword)]
        [MaxLength(50, ErrorMessage = ErrorMessage.MaxLengthPassword)]
        public string Password { get; set; } // P@ssword1234

        public bool RememberMe { get; set; }
    }
}