﻿using BlogEngine.Assignment.Web.BaseViewModels;
using System;

namespace BlogEngine.Assignment.Web.ViewModels
{
    public class CommentViewModel : AuditViewModel
    {
        public Guid CommentId { get; set; }

        public int ID { set; get; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Content { get; set; }

        public string Email { get; set; }

        public int PostID { get; set; }

        public int CountLikes { get; set; } = 0;

        public int CountPostComments { get; set; } = 0;

        public int CountReplyComments { get; set; } = 0;
       
        public virtual PostViewModel Post { get; set; }
    }
}
