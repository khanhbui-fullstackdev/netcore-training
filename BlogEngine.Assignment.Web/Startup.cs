﻿using AutoMapper;
using BlogEngine.Assignment.Common.Constants;
using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.DataAccess.Repositories;
using BlogEngine.Assignment.DataAccess.Repositories.IRepositories;
using BlogEngine.Assignment.Entities;
using BlogEngine.Assignment.Service;
using BlogEngine.Assignment.Service.IServices;
using BlogEngine.Assignment.Service.Mappings;
using BlogEngine.Assignment.Web.Mappings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;

namespace BlogEngine.Assignment.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // This method configures the app's services.
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            ConfigureSession(services);
            ConfigureDependencyUnitOfWorkAndDbFactory(services);
            ConfigureConnectionString(services);
            ConfigureDependencyRepositories(services);
            ConfigureDependencyServices(services);
            ConfigureAutoMapper(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //  Exception / error handling
            //  HTTP Strict Transport Security Protocol
            //  HTTPS redirection
            //  Static file server
            //  Cookie policy enforcement
            //  Authentication
            //  Session
            //  MVC
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseSession();
            ConfigureRegisterRoutes(app);
        }

        #region Configure Dependency Unit Of Work/DbFactory
        public void ConfigureDependencyUnitOfWorkAndDbFactory(IServiceCollection services)
        {
            // Transient lifetime services are created each time they're requested. 
            // This lifetime works best for lightweight, stateless services.
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IDbFactory, DbFactory>();
        }
        #endregion

        #region Configure Connection String
        /// <summary>
        /// Configure BlogEngine Db Context
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureConnectionString(IServiceCollection services)
        {
            services.AddDbContext<BlogEngineDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString(Config.BlogEngineConnectionString)));
        }
        #endregion

        #region Configure Register Routes
        public void ConfigureRegisterRoutes(IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute
                (
                    name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Home", action = "GetAllPosts" }
                );
                routes.MapRoute
                (
                    name: "Post",
                    template: "post/{slug}/{id}",
                    defaults: new { controller = "Post", action = "GetPostById" }
                );
                routes.MapRoute
                (
                    name: "Posts By Category",
                    template: "category/{slug}/{id}",
                    defaults: new { controller = "Post", action = "GetPostsByCategory" }
                );
                routes.MapRoute
                (
                    name: "Posts By Sub Category",
                    template: "subcategory/{slug}/{id}",
                    defaults: new { controller = "Post", action = "GetPostsBySubcategory" }
                );
                routes.MapRoute
                (
                    name: "Search Result",
                    template: "posts/search-result/",
                    defaults: new { controller = "Post", action = "GetPostsByKeyword" }
                );
            });
        }
        #endregion

        #region Configure Dependency Services
        public void ConfigureDependencyServices(IServiceCollection services)
        {
            // Scoped lifetime services are created once per request.
            // Add application services.
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ISubCategoryService, SubCategoryService>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<ICommentService, CommentService>();
        }
        #endregion

        #region Configure Dependency Repositories
        public void ConfigureDependencyRepositories(IServiceCollection services)
        {
            // Add application repositories
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ISubCategoryRepository, SubCategoryRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
        }
        #endregion

        #region Configure Auto Mapper
        public void ConfigureAutoMapper(IServiceCollection services)
        {
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperDtoAndEntityConfiguration());
                mc.AddProfile(new AutoMapperDtoAndViewModelConfiguration());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
        #endregion

        #region Configure Session
        public void ConfigureSession(IServiceCollection services)
        {
            services.AddSession(options =>
            {
                options.Cookie.Name = ".AdventureWorks.Session";
            });
            services.AddDistributedMemoryCache();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }
        #endregion

        #region Config ASP.NET Identity
        public void ConfigIdentity(IServiceCollection services)
        {
            services.AddDefaultIdentity<IdentityUser>()
               .AddEntityFrameworkStores<BlogEngineDbContext>()
               .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 12;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

        }
        #endregion
    }
}
