﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BlogEngine.Assignment.Common.Constants;
using BlogEngine.Assignment.Common.Enums;
using BlogEngine.Assignment.Service.IServices;
using BlogEngine.Assignment.Web.Infrastructure.Cores;
using BlogEngine.Assignment.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BlogEngine.Assignment.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IPostService _postService;
        private readonly ICategoryService _categoryService;

        private readonly int _pageSize = 5;
        private readonly int _maxPage = 50;
        public HomeController(
            ICategoryService categoryService,
            IPostService postService,
            IMapper mapper)
        {
            this._categoryService = categoryService;
            this._postService = postService;
            this._mapper = mapper;
        }

        public async Task<IActionResult> GetAllPosts(int page = 1, SortingType sortingType = SortingType.Default)
        {
            var tuple = await _postService.GetAllPosts(page, _pageSize, sortingType);
            var postsDto = tuple.Item1;
            var totalRows = tuple.Item2;

            int totalPage = (int)Math.Ceiling((double)totalRows / _pageSize);
            var postsViewModel = _mapper.Map<IEnumerable<PostViewModel>>(postsDto);

            var paginationSet = new PaginationSet<PostViewModel>()
            {
                Items = postsViewModel,
                MaxPage = _maxPage,
                Page = page,
                TotalCount = totalRows,
                TotalPages = totalPage,
            };
            PaginationSetViewModel postPaginationViewModel = new PaginationSetViewModel
            {
                PostPaginationSet = paginationSet
            };
            return View(ViewUrl.HomeIndex, postPaginationViewModel);
        }

        [HttpGet]
        public async Task<JsonResult> GetCategories()
        {
            try
            {
                var categoriesDto = await _categoryService.GetAllCategoriesWithSubCategories();
                var categoriesViewModel = _mapper.Map<IEnumerable<CategoryViewModel>>(categoriesDto);

                TreeViewModel treeViewModel = new TreeViewModel();
                List<TreeViewModel> treeViewModels = treeViewModel.TreeViewMappingCategory(categoriesViewModel);

                var jsonData = JsonConvert.SerializeObject
                (
                    treeViewModels,
                    Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }
                );

                jsonData = jsonData.Replace("_", "data-");

                return Json(new
                {
                    status = true,
                    data = jsonData
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    error = ex.InnerException.Message
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetPostsByKeyword(string keyword)
        {
            try
            {
                var postsDto = await _postService.GetPostsByKeyword(keyword);
                var postsViewModel = _mapper.Map<IEnumerable<PostViewModel>>(postsDto);

                var categoriesDto = await _categoryService.GetCategoriesByKeyword(keyword);
                var categoriesViewModel = _mapper.Map<IEnumerable<CategoryViewModel>>(categoriesDto);

                SearchResultViewModel searchResultViewModel = new SearchResultViewModel
                {
                    Posts = postsViewModel,
                    Categories = categoriesViewModel
                };
                return Json(searchResultViewModel);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    error = ex.InnerException.Message
                });
            }
        }
    }
}
