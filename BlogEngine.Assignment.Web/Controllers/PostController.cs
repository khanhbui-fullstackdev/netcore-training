﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BlogEngine.Assignment.Common.Constants;
using BlogEngine.Assignment.Common.Enums;
using BlogEngine.Assignment.Service.IServices;
using BlogEngine.Assignment.Web.Infrastructure.Cores;
using BlogEngine.Assignment.Web.ViewModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BlogEngine.Assignment.Web.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostService _postService;
        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;

        private readonly int _pageSize = 5;
        private readonly int _maxPage = 100;
        private const string _SessionKeyword = "keyword";

        public PostController(
            IPostService postService,
            ICommentService commentService,
            IMapper mapper)
        {
            this._postService = postService;
            this._commentService = commentService;
            this._mapper = mapper;
        }

        /// <summary>
        /// GetPostById - view post detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> GetPostById(int id)
        {
            var postDto = await _postService.GetPostById(id);
            var postViewModel = _mapper.Map<PostViewModel>(postDto);

            var commentsDto = await _commentService.GetCommentsByPostId(id);
            var commentsViewModel = _mapper.Map<IEnumerable<CommentViewModel>>(commentsDto);

            PostCommentViewModel postCommentViewModel = new PostCommentViewModel
            {
                Comments = commentsViewModel,
                Post = postViewModel
            };
            ViewBag.CategoryId = postViewModel.CategoryID;
            return View(ViewUrl.PostIndex, postCommentViewModel);
        }

        public async Task<IActionResult> GetPostsByCategory(int id, int page = 1, SortingType sortingType = SortingType.Default)
        {
            var tuple = await _postService.GetPostsByCategory(id, page, _pageSize, sortingType);
            var postsDto = tuple.Item1;
            var totalRows = tuple.Item2;

            int totalPage = (int)Math.Ceiling((double)totalRows / _pageSize);
            var postsViewModel = _mapper.Map<IEnumerable<PostViewModel>>(postsDto);

            var paginationSet = new PaginationSet<PostViewModel>()
            {
                Items = postsViewModel,
                MaxPage = _maxPage,
                Page = page,
                TotalCount = totalRows,
                TotalPages = totalPage,
            };
            PaginationSetViewModel postPaginationViewModel = new PaginationSetViewModel
            {
                PostPaginationSet = paginationSet
            };

            ViewBag.CategoryId = id;
            return View(ViewUrl.PostsByCategory, postPaginationViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetPostsByKeyword(string keyword, int page = 1, SortingType sortingType = SortingType.Default)
        {
            //  using Microsoft.AspNetCore.Http;
            if (string.IsNullOrEmpty(HttpContext.Session.GetString(_SessionKeyword)))
            {
                HttpContext.Session.SetString(_SessionKeyword, keyword);
            }
            else
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    IdleTimeout = TimeSpan.FromMinutes(1)
                };
            }

            var currentAction = ControllerContext.ActionDescriptor.ActionName;
            if (!currentAction.Equals(nameof(GetPostsByKeyword)))
            {
                // Clear session
                HttpContext.Session.Clear();
            }
            var sessionValue = HttpContext.Session.GetString(_SessionKeyword);

            var tuple = await _postService.GetPostsByKeyword(sessionValue, page, _pageSize, sortingType);
            var postsDto = tuple.Item1;
            var totalRows = tuple.Item2;

            int totalPage = (int)Math.Ceiling((double)totalRows / _pageSize);
            var postsViewModel = _mapper.Map<IEnumerable<PostViewModel>>(postsDto);
           
            var paginationSet = new PaginationSet<PostViewModel>()
            {
                Items = postsViewModel,
                MaxPage = _maxPage,
                Page = page,
                TotalCount = totalRows,
                TotalPages = totalPage,
                Keyword = sessionValue,
                Sorting = sortingType
            };
            PaginationSetViewModel paginationViewModel = new PaginationSetViewModel
            {
                PostPaginationSet = paginationSet
            };

            return View(ViewUrl.PostsByKeyword, paginationViewModel);
        }

        public async Task<IActionResult> GetPostsBySubcategory(int id, int page = 1, SortingType sortingType = SortingType.Default)
        {
            var tuple = await _postService.GetPostsBySubcategory(id, page, _pageSize, sortingType);
            var postsDto = tuple.Item1;
            var totalRows = tuple.Item2;

            int totalPage = (int)Math.Ceiling((double)totalRows / _pageSize);
            var postsViewModel = _mapper.Map<IEnumerable<PostViewModel>>(postsDto);

            var paginationSet = new PaginationSet<PostViewModel>()
            {
                Items = postsViewModel,
                MaxPage = _maxPage,
                Page = page,
                TotalCount = totalRows,
                TotalPages = totalPage,
            };
            PaginationSetViewModel postPaginationViewModel = new PaginationSetViewModel
            {
                PostPaginationSet = paginationSet
            };
            ViewBag.SubCategoryId = id;
            return View(ViewUrl.PostsBySubCategory, postPaginationViewModel);
        }
    }
}
