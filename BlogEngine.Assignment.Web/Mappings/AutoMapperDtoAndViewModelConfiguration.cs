﻿using AutoMapper;
using BlogEngine.Assignment.Dto;
using BlogEngine.Assignment.Dto.BaseDto;
using BlogEngine.Assignment.Web.BaseViewModels;
using BlogEngine.Assignment.Web.ViewModels;

namespace BlogEngine.Assignment.Web.Mappings
{
    public class AutoMapperDtoAndViewModelConfiguration : Profile
    {
        public AutoMapperDtoAndViewModelConfiguration()
        {
            CreateMap<AuditDto, AuditViewModel>();
            CreateMap<CategoryDto, CategoryViewModel>();
            CreateMap<SubCategoryDto, SubCategoryViewModel>();
            CreateMap<PostDto, PostViewModel>();
            CreateMap<CommentDto, CommentViewModel>();
            CreateMap<ReplyCommentDto, ReplyCommentDto>();
        }
    }
}
