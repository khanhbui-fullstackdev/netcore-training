﻿using System.Collections.Generic;

namespace BlogEngine.Assignment.Web.BaseViewModels
{
    public abstract class TreeViewBase
    {
        public string Text { get; set; }
        public string Href { get; set; }
        public List<int> Tags { get; set; }
    }
}
