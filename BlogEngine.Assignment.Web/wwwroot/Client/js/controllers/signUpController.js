﻿var signUpController = {
    init: function () {
        signUpController.registerEvents();
        signUpController.customRules();
    },
    registerEvents: function () {
        var frm_registerAccountConfig = {
            rules: {
                firstName: {
                    required: true,
                    minlength: 2,
                    maxlength: 255,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                lastName: {
                    required: true,
                    minlength: 2,
                    maxlength: 255,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                birthday: {
                    required: true,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                email: {
                    required: true,
                    email: true,
                    minlength: 12,
                    maxlength: 50,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                username: {
                    required: true,
                    minlength: 6,
                    maxlength: 50,
                    //userNameExist: true,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                password: {
                    required: true,
                    minlength: 10,
                    maxlength: 50,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                confirmPassword: {
                    required: true,
                    minlength: 10,
                    maxlength: 50,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true,
                    comparePassword: true
                },
                CaptchaCode: {
                    required: true,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                }
            },
            messages: {
                email: {
                    required: 'Email is required',
                    email: 'Email is invalid',
                    minlength: 'Email requires at least 12 characters',
                    maxlenght: 'Email cannot length over 50 characters',
                    preventScriptInjection: 'Email cannot contain any javascript code',
                    preventHtmlInjection: 'Email cannot contain any html characters',
                    preventWhiteSpace: 'Email cannot contain any white space or empty string'
                },
                firstName: {
                    required: 'First name is required',
                    minlength: 'First name requires at least 2 characters',
                    maxlength: 'First name cannot length over 255 characters',
                    preventScriptInjection: 'First name cannot contain any javascript code',
                    preventHtmlInjection: 'First name cannot contain any html characters',
                    preventWhiteSpace: 'First name cannot contain any white space or empty string'
                },
                lastName: {
                    required: 'Last name is required',
                    minlength: 'Last name requires at least 2 characters',
                    maxlength: 'Last name cannot length over 255 characters',
                    preventScriptInjection: 'Last name cannot contain any javascript code',
                    preventHtmlInjection: 'Last name cannot contain any html characters',
                    preventWhiteSpace: 'Last name cannot contain any white space or empty string'
                },
                birthDay: {
                    required: 'Birthday is required',
                    preventScriptInjection: 'Birthday cannot contain any javascript code',
                    preventHtmlInjection: 'Birthday cannot contain any html characters',
                    preventWhiteSpace: 'Birthday cannot contain any white space or empty string'
                },
                userName: {
                    required: 'Username is required',
                    minlength: 'Username requires at least 6 characters',
                    maxlength: 'Username cannot length over 50 characters',
                    //userNameExist: 'Username is already taken',
                    preventScriptInjection: 'Username cannot contain any javascript code',
                    preventHtmlInjection: 'Username cannot contain any html characters',
                    preventWhiteSpace: 'Username cannot contain any white space or empty string'
                },
                password: {
                    required: 'Password is required',
                    minlength: 'Password requires at least 10 characters',
                    maxlength: 'Password cannot length over 50 characters',
                    preventScriptInjection: 'Password cannot contain any javascript code',
                    preventHtmlInjection: 'Password cannot contain any html characters',
                    preventWhiteSpace: 'Password cannot contain any white space or empty string'
                },
                confirmPassword: {
                    required: 'Confirm password is required',
                    minlength: 'Confirm password requires at least 10 characters',
                    maxlength: 'Confirm password cannot length over 50 characters',
                    preventScriptInjection: 'Confirm password cannot contain any javascript code',
                    preventHtmlInjection: 'Confirm password cannot contain any html characters',
                    preventWhiteSpace: 'Confirm password cannot contain any white space or empty string',
                    comparePassword: 'Password must match with confirm password'
                },
                CaptchaCode: {
                    required: 'CaptchaCode is required',
                    preventScriptInjection: 'CaptchaCode cannot contain any javascript code',
                    preventHtmlInjection: 'CaptchaCode cannot contain any html characters',
                    preventWhiteSpace: 'CaptchaCode cannot contain any white space or empty string'
                }
            }
        };
        $('#txtFirstName').off('keyup').on('keyup', function (event) {
            var firstNameValid = $('#txtFirstName').valid();
            switch (firstNameValid) {
                case true:
                    $('#txtFirstName').removeClass('input-fields-textbox-error');
                    $('#txtFirstName-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtFirstName').addClass('input-fields-textbox-error');
                    $('#txtFirstName-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#txtLastName').off('keyup').on('keyup', function (event) {
            var lastNameValid = $('#txtLastName').valid();
            switch (lastNameValid) {
                case true:
                    $('#txtLastName').removeClass('input-fields-textbox-error');
                    $('#txtLastName-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtLastName').addClass('input-fields-textbox-error');
                    $('#txtLastName-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#txtEmail').off('keyup').on('keyup', function (event) {
            var emailValid = $('#txtEmail').valid();
            switch (emailValid) {
                case true:
                    $('#txtEmail').removeClass('input-fields-textbox-error');
                    $('#txtEmail-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtEmail').addClass('input-fields-textbox-error');
                    $('#txtEmail-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#txtUserName').off('keyup').on('keyup', function (event) {
            event.preventDefault();
            var usernameValid = $('#txtUserName').valid();
            switch (usernameValid) {
                case true:
                    $('#txtUserName').removeClass('input-fields-textbox-error');
                    $('#txtUserName-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtUserName').addClass('input-fields-textbox-error');
                    $('#txtUserName-error').addClass('input-fields-label-error');
                    break;
            }
        });
        //$('#txtUserName').off('change').on('change', function (event) {
        //    event.preventDefault();
        //    var username = $('#txtUserName').val();
        //    console.log("Username:" + username);
        //    if (username.length >= 6) {
        //        $.when(signUpController.checkUserNameExist(username)).then(function (result) {
        //            if (result.status == true && result.userExist == true) {
        //                // Username is already taken
        //                $("#txtUserName-error").text('Username is already taken');
        //                $('#txtUserName').addClass('input-fields-textbox-error');
        //                $('#txtUserName-error').addClass('input-fields-label-error');
        //            } else {
        //                $('#txtUserName').removeClass('input-fields-textbox-error');
        //                $('#txtUserName-error').removeClass('input-fields-label-error');
        //            }
        //        });
        //    }
        //});
        $('#txtPassword').off('keyup').on('keyup', function (event) {
            var passwordValid = $('#txtPassword').valid();
            switch (passwordValid) {
                case true:
                    $('#txtPassword').removeClass('input-fields-textbox-error');
                    $('#txtPassword-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtPassword').addClass('input-fields-textbox-error');
                    $('#txtPassword-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#txtConfirmPassword').off('keyup').on('keyup', function (event) {
            var confirmPassword = $('#txtConfirmPassword').valid();
            switch (confirmPassword) {
                case true:
                    $('#txtConfirmPassword').removeClass('input-fields-textbox-error');
                    $('#txtConfirmPassword-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtConfirmPassword').addClass('input-fields-textbox-error');
                    $('#txtConfirmPassword-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#txtCaptchaCode').off('keyup').on('keyup', function (event) {
            var captchaCodeValid = $('#txtCaptchaCode').valid();
            switch (captchaCodeValid) {
                case true:
                    $('#txtCaptchaCode').removeClass('input-fields-textbox-error');
                    $('#txtCaptchaCode-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtCaptchaCode').addClass('input-fields-textbox-error');
                    $('#txtCaptchaCode-error').addClass('input-fields-label-error');
                    break;
            }
        });

        $('#txtBirthDay').datepicker({
            dateFormat: "dd/M/yy",
            changeMonth: true,
            changeYear: true,
            defaultDate: null,
            yearRange: "-60:+0",
            onSelect: function (dateText, inst) {
                $("#txtBirthDay").datepicker("setDate", new Date(inst.selectedYear, inst.selectedMonth, parseInt(inst.selectedDay)));
            }
        });
        var today = new Date();
        var currentDate = $("#txtBirthDay").datepicker("getDate");
        if (!currentDate) {
            $("#txtBirthDay").datepicker("setDate", today);
        }

        $('#frm_registerAccount').validate(frm_registerAccountConfig);

        // SignUp Account
        $('#btnSignupAccount').off('click').on('click', function (e) {
            e.preventDefault();
            var isValid = $('#frm_registerAccount').valid();
            if (isValid) {
                var firstName = $('#txtFirstName').val();
                var lastName = $('#txtLastName').val();
                var email = $('#txtEmail').val();
                var username = $('#txtUserName').val();
                var password = $('#txtPassword').val();
                var confirmPassword = $('#txtConfirmPassword').val();
                var birthDay = $('#txtBirthDay').val();
                var applicationUser = {
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    userName: username,
                    password: password,
                    confirmPassword: confirmPassword,
                    birthDay: birthDay
                };

                signUpController.checkCaptcha(applicationUser);

            } else {
                // Show all invalid messages
                signUpController.validatedFields();
            }
        });
    },
    clearText: function () {
        $('#txtFirstName').val('');
        $('#txtLastName').val('');
        $('#txtEmail').val('');
        $('#txtUserName').val('');
        $('#txtPassword').val('');
        $('#txtConfirmPassword').val('');
        $('#txtCaptchaCode').val('');
    },
    validatedFields: function () {
        var firstNameValid = $('#txtFirstName').valid();
        var lastNameValid = $('#txtLastName').valid();
        var emailValid = $('#txtEmail').valid();
        var userNameValid = $('#txtUserName').valid();
        var passwordValid = $('#txtPassword').valid();
        var confirmPasswordValid = $('#txtConfirmPassword').valid();
        var captchaValid = $('#txtCaptchaCode').valid();

        switch (firstNameValid) {
            case true:
                $('#txtFirstName').removeClass('input-fields-textbox-error');
                $('#txtFirstName-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtFirstName').addClass('input-fields-textbox-error');
                $('#txtFirstName-error').addClass('input-fields-label-error');
                break;
        }
        switch (lastNameValid) {
            case true:
                $('#txtLastName').removeClass('input-fields-textbox-error');
                $('#txtLastName-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtLastName').addClass('input-fields-textbox-error');
                $('#txtLastName-error').addClass('input-fields-label-error');
                break;
        }
        switch (emailValid) {
            case true:
                $('#txtEmail').removeClass('input-fields-textbox-error');
                $('#txtEmail-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtEmail').addClass('input-fields-textbox-error');
                $('#txtEmail-error').addClass('input-fields-label-error');
                break;
        }
        switch (userNameValid) {
            case true:
                $('#txtUserName').removeClass('input-fields-textbox-error');
                $('#txtUserName-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtUserName').addClass('input-fields-textbox-error');
                $('#txtUserName-error').addClass('input-fields-label-error');
                break;
        }
        switch (passwordValid) {
            case true:
                $('#txtPassword').removeClass('input-fields-textbox-error');
                $('#txtPassword-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtPassword').addClass('input-fields-textbox-error');
                $('#txtPassword-error').addClass('input-fields-label-error');
                break;
        }
        switch (confirmPasswordValid) {
            case true:
                $('#txtConfirmPassword').removeClass('input-fields-textbox-error');
                $('#txtConfirmPassword-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtConfirmPassword').addClass('input-fields-textbox-error');
                $('#txtConfirmPassword-error').addClass('input-fields-label-error');
                break;
        }
        switch (captchaValid) {
            case true:
                $('#txtCaptchaCode').removeClass('input-fields-textbox-error');
                $('#txtCaptchaCode-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtCaptchaCode').addClass('input-fields-textbox-error');
                $('#txtCaptchaCode-error').addClass('input-fields-label-error');
                break;
        }
    },
    checkEmailExist: function (email) {
        var ajaxConfig = {
            url: "/Account/CheckEmailExist",
            data: {
                email: email
            },
            type: "POST",
            dataType: 'JSON',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (response, status, xhr) {
                if (response.status) {
                    // Do something here
                    console.log(response);
                } else {
                    // Throw message username is exist in database

                }
            }, error: function (xhr, status, error) {
                toastr.error(error, 'Error');
            }
        };
        $.ajax(ajaxConfig);
    },
    checkUserNameExist: function (username) {
        /** 
         *  https://stackoverflow.com/questions/3302702/jquery-return-value-using-ajax-result-on-success
         * 
         *  Deferred object
         *  https://stackoverflow.com/questions/4866721/what-are-deferred-objects
         */
        var dfd = $.Deferred();
        var ajaxConfig = {
            url: "/Account/CheckUserNameExist",
            data: {
                username: username
            },
            type: "POST",
            dataType: 'JSON',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (response, status, xhr) {
                dfd.resolve(response);
            }, error: function (error) {
                toastr.error(error, 'Error');
            }
        };
        $.ajax(ajaxConfig);
        return dfd.promise();
    },
    checkCaptcha: function (applicationUser) {
        // get client-side Captcha object instance
        var captchaObj = $("#txtCaptchaCode").get(0).Captcha;

        // gather data required for Captcha validation
        var params = {};
        params.CaptchaId = captchaObj.Id;
        params.InstanceId = captchaObj.InstanceId;
        params.UserInput = $("#txtCaptchaCode").val();

        var url = '/Account/CheckCaptcha';
        $.getJSON(url, params, function (result) {
            if (result === true) {
                signUpController.signUp(applicationUser);
            } else {
                $('#lblCaptcha').show();
                captchaObj.ReloadImage();
            }
        });
        event.preventDefault();
    },
    signUp: function (applicationUser) {
        var ajaxConfig = {
            url: "/Account/SignUp",
            data: {
                signUpInfo: JSON.stringify(applicationUser)
            },
            type: "POST",
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            beforeSend: function () {
                $('#wait').show();
            },
            success: function (response, status, xhr) {
                if (response.status) {
                    $('#txtEmail-error').hide();
                    $('#txtUserName-error').hide();

                    toastr.success('Your account has been signup successfully. Please check your email to activate your account', 'Success');
                    signUpController.clearText();
                } else {
                    switch (response.emailExist) {
                        case true:
                            $('#txtEmail-error').show();
                            $('#txtEmail-error').text(response.error);
                            $('#txtEmail-error').addClass('input-fields-label-error');
                            $('#txtEmail').addClass('input-fields-textbox-error');
                            break;
                        case false:
                            $('#txtEmail-error').hide();
                            $('#txtEmail-error').removeClass('input-fields-label-error');
                            $('#txtEmail').removeClass('input-fields-textbox-error');
                            break;
                        default:
                            $('#txtEmail-error').text('');
                            $('#txtEmail-error').hide();
                            $('#txtEmail-error').removeClass('input-fields-label-error');
                            $('#txtEmail').removeClass('input-fields-textbox-error');
                    }

                    switch (response.usernameExist) {
                        case true:
                            $('#txtUserName-error').show();
                            $('#txtUserName-error').text(response.error);
                            $('#txtUserName-error').addClass('input-fields-label-error');
                            $('#txtUserName').addClass('input-fields-textbox-error');
                            break;
                        case false:
                            $('#txtUserName-error').text('');
                            $('#txtUserName-error').hide();
                            $('#txtUserName-error').removeClass('input-fields-label-error');
                            $('#txtUserName').removeClass('input-fields-textbox-error');
                            break;
                        default:
                            $('#txtUserName-error').text('');
                            $('#txtUserName-error').hide();
                            $('#txtUserName-error').removeClass('input-fields-label-error');
                            $('#txtUserName').removeClass('input-fields-textbox-error');
                    }
                }
            }, error: function (xhr, status, error) {
                toastr.error(error, 'Error');
            },
            complete: function (xhr, textStatus) {
                // A function to run when the request is finished (after success and error functions)
                $('#wait').hide();
            }
        };
        $.ajax(ajaxConfig);
    },
    customRules: function () {
        jQuery.validator.addMethod('preventScriptInjection', function (value, element, params) {
            if (value.toLowerCase().search('<script>') !== -1 || value.toLowerCase().search('</script>') !== -1)
                return false; // has script
            return true; // none script
        }, '');
        jQuery.validator.addMethod('preventHtmlInjection', function (value, element, params) {
            var htmlPattern = new RegExp(/<[a-z][\s\S]*>/i);
            if (htmlPattern.test(value))
                return false; // has html
            return true; // none html
        }, '');
        jQuery.validator.addMethod('preventWhiteSpace', function (value, element, params) {
            if (value.trim() === '')
                return false; // has empty string
            return true; // none empty string
        }, '');
        jQuery.validator.addMethod('comparePassword', function (value, element, params) {
            var password = $('#txtPassword').val();
            var repeatNewPassword = $('#txtConfirmPassword').val();
            if (password && repeatNewPassword) {
                if (password !== repeatNewPassword)
                    return false; // password doesn't match
                return true; // match password
            } else return true;
        }, '');
        jQuery.validator.addMethod('userNameExist', function (value, element, params) {
            if (value.length >= 6) {
                $.when(signUpController.checkUserNameExist(value)).then(function (result) {
                    console.log('result:' + result);
                    if (result.status === true && result.userExist === true)
                        return false; // has user exist
                    return true; //none user exist
                });
            } else return true;
        }, '');
    }
};
signUpController.init();

