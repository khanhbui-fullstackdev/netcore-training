﻿var likeCommentController = {
    init: function () {
        likeCommentController.registerEvents();
    },
    registerEvents: function () {
        // Event binding
        $('.btnLikeReplyComment').off('click').on('click', function (e) {
            e.preventDefault();
            var isSignedIn = $('#txtIsSignedIn').val();
            if (!isSignedIn || isSignedIn !== 'value')
                commonController.signIn();
            else {
                var replyCommentId = $(this).attr('id').split('_')[1];
                var userName = $('#a-userName').text().trim();
                var countLikeReplyComments = 0;
                countLikeReplyComments++;

                var likeReplyComment = {
                    userName: userName,
                    replyCommentId: replyCommentId,
                    countLikeReplyComments: countLikeReplyComments,
                    likeType: 1
                };
                likeCommentController.addNewLikeReplyComment(likeReplyComment);
            }
        });
    },
    addNewLikeReplyComment: function (likeReplyComment) {
        var replyCommentId = likeReplyComment['replyCommentId'];
        var ajaxConfig = {
            url: '/LikeComment/AddNewLike',
            type: 'POST',
            dataType: 'JSON',
            data: {
                likeCommentInfo: JSON.stringify(likeReplyComment)
            },
            beforeSend: function () {
                $('#wait').show();
            },
            success: function (response, status, xhr) {
                if (response.status) {
                    var totalLikes = response.totalLikes;
                    if (totalLikes > 1) {
                        $('#small-numberOfReplyCommentLikes_' + replyCommentId).text(totalLikes + ' Likes');
                    } else {
                        $('#small-numberOfReplyCommentLikes_' + replyCommentId).text(totalLikes + ' Like');
                    }
                } else {
                    toastr.error(error);
                }
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            },
            complete: function (xhr, textStatus) {
                // A function to run when the request is finished (after success and error functions)
                $('#wait').hide();
            }
        };
        $.ajax(ajaxConfig);
    }
};
likeCommentController.init();