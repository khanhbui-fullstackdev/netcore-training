﻿var signInController = {
    init: function () {
        signInController.registerEvents();
    },
    registerEvents: function () {        
        // Validate fields for signIn
        var frm_signInConfig = {
            rules: {
                usernameOrEmail: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                usernameOrEmail: {
                    required: 'Username or email is required'
                },
                password: {
                    required: 'Password is required'
                }
            }
        };
        $('#frm_signIn').validate(frm_signInConfig);
        //Starting to implement Keyup event form validate
        signInController.keyUpEventFormSignIn();
        //Ending to implement keyup event form validate

        $('#btnSignIn').off('click').on('click', function (e) {
            e.preventDefault();
            var isValid = $('#frm_signIn').valid();
            if (isValid) {
                $('#frm_signIn').submit();
            } else {
                // Show all invalid messages
                signInController.validateFieldsFormSignIn();
            }
        });
        $('#btnCancel').off('click').on('click', function (e) {
            e.preventDefault();
            // Close Signin dialog
            $('#frm_signInDialog').dialog('close');
        });
    },
    validateFieldsFormSignIn: function () {
        var emailOrUserNameValid = $('#txtSignInUsernameOrEmail').valid();
        var passwordValid = $('#txtSignInPassword').valid();

        switch (emailOrUserNameValid) {
            case true:
                $('#txtSignInUsernameOrEmail').removeClass('input-fields-textbox-error');
                $('#txtSignInUsernameOrEmail-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtSignInUsernameOrEmail').addClass('input-fields-textbox-error');
                $('#txtSignInUsernameOrEmail-error').addClass('input-fields-label-error');
                break;
        }
        switch (passwordValid) {
            case true:
                $('#txtSignInPassword').removeClass('input-fields-textbox-error');
                $('#txtSignInPassword-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtSignInPassword').addClass('input-fields-textbox-error');
                $('#txtSignInPassword-error').addClass('input-fields-label-error');
                break;
        }
    },
    keyUpEventFormSignIn: function () {
        $('#txtSignInUsernameOrEmail').off('keyup').on('keyup', function (event) {
            var userNameOrEmailValid = $('#txtSignInUsernameOrEmail').valid();
            switch (userNameOrEmailValid) {
                case true:
                    $('#txtSignInUsernameOrEmail').removeClass('input-fields-textbox-error');
                    $('#txtSignInUsernameOrEmail-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtSignInUsernameOrEmail').addClass('input-fields-textbox-error');
                    $('#txtSignInUsernameOrEmail-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#txtSignInPassword').off('keyup').on('keyup', function (event) {
            var signInPasswordValid = $('#txtSignInPassword').valid();
            switch (signInPasswordValid) {
                case true:
                    $('#txtSignInPassword').removeClass('input-fields-textbox-error');
                    $('#txtSignInPassword-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtSignInPassword').addClass('input-fields-textbox-error');
                    $('#txtSignInPassword-error').addClass('input-fields-label-error');
                    break;
            }
        });
    },
    customRules: function () {
        jQuery.validator.addMethod('preventScriptInjection', function (value, element, params) {
            if (value.toLowerCase().search('<script>') !== -1 || value.toLowerCase().search('</script>') !== -1)
                return false; // has script
            return true; // none script
        }, '');
        jQuery.validator.addMethod('preventHtmlInjection', function (value, element, params) {
            var htmlPattern = new RegExp(/<[a-z][\s\S]*>/i);
            if (htmlPattern.test(value))
                return false; // has html
            return true; // none html
        }, '');
        jQuery.validator.addMethod('preventWhiteSpace', function (value, element, params) {
            if (value.trim() === '')
                return false; // has empty string
            return true; // none empty string
        }, '');
    }
};
signInController.init();
