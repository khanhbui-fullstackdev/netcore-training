﻿var commonController = {
    init: function () {
        commonController.getPostsByKeyword();
        commonController.registerEvents();
    },
    registerEvents: function () {
        $("a[name='signIn']").off('click').on('click', function (e) {
            e.preventDefault();
            commonController.signIn();
        });
        $('#btnSignOut').off('click').on('click', function (e) {
            e.preventDefault();
            $('#frm_signOut').submit();
        });
    },
    getPostsByKeyword: function () {
        $.typeahead({
            input: ".js-typeahead",
            order: "asc",
            hint: true,
            minLength: 4,
            group: {
                template: "{{group}}"
            },
            maxItemPerGroup: 5,
            dynamic: true,
            template: function (query, item) {
                var postName = item.name === null || item.name === undefined ? '' : item.name;
                var categoryName = (item.categoryName === null || item.categoryName === undefined) ? '</small></div>' : item.categoryName;
                var templateHtml = "<div class='suggest-item post-suggestion'>" +
                    "<i class='fa fa-file-text-o' aria-hidden='true'></i> &nbsp;" + postName +
                    "<br />&nbsp; &nbsp; &nbsp; <small class='post-suggestion-text'> &nbsp;" + categoryName + "</small></div>";
                return templateHtml;
            },
            source: {
                "posts": {
                    ajax: {
                        url: "/Home/GetPostsByKeyword",
                        path: "posts",
                        data: { keyword: "{{query}}" }
                    },
                    display: "name"
                },
                "categories": {
                    ajax: {
                        url: "/Home/GetPostsByKeyword",
                        path: "categories",
                        data: { keyword: "{{query}}" }
                    },
                    display: "name"
                }
            },
            callback: {
                onClickAfter: function (node, a, item, event) {
                   
                }
            }
        });
    },
    signIn: function () {
        //Login dialog popup
        var frm_signInDialogConfig = {
            autoOpen: false,
            height: 550,
            width: 500,
            modal: true,
            classes: {
                'ui-dialog-titlebar-close': 'ui-icon ui-icon-closethick'
            },
            close: function (event, ui) {
                // Do something here
            }
        };
        $("#frm_signInDialog").dialog(frm_signInDialogConfig);

        var ajaxConfig = {
            url: '/Account/SignIn',
            type: 'GET',
            dataType: 'html',
            data: {
                returnUrl: window.location.href
            },
            success: function (result, status, xhr) {
                $('#frm_signInDialog').html(result);
                $('#frm_signInDialog').dialog('open');
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            }
        };
        $.ajax(ajaxConfig);
    }
};
commonController.init();//call itself