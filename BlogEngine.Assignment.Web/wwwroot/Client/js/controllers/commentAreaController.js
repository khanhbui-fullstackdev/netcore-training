﻿var commentAreaController = {
    init: function () {
        commentAreaController.registerEvents();
    },
    registerEvents: function () {
        $('.btnReplyComment').off('click').on('click', function (e) {
            e.preventDefault();
            var commentId = $(this).attr('id').split('_')[1];
            $('.reply').html('');
            $('#comment-text').hide();
            commentAreaController.getAllReplyComments(commentId);
        });
        // Show all reply comments
        $('.small-numberOfReplyComments').off('click').on('click', function (e) {
            e.preventDefault();
            var commentId = $(this).attr('id').split('_')[1];
            $('.reply').html('');
            $('#comment-text').hide();
            commentAreaController.getAllReplyComments(commentId);
        });
        // Like comments
        $('.btnLikeComment').off('click').on('click', function (e) {
            e.preventDefault();
            var isSignedIn = $('#txtIsSignedIn').val();
            if (!isSignedIn || isSignedIn !== 'value')
                commonController.signIn();
            else {
                var commentId = $(this).attr('id').split('_')[1];
                var userName = $('#a-userName').text().trim();
                var countLikeComments = 0;
                countLikeComments++;

                var likeComment = {
                    userName: userName,
                    commentId: commentId,
                    countLikeComments: countLikeComments,
                    likeType: 0
                };
                commentAreaController.addNewLikeComment(likeComment);
            }
        });
    },
    getAllReplyComments: function (commentId) {
        var ajaxConfig = {
            url: '/ReplyComment/GetAllReplyComments',
            type: 'POST',
            dataType: 'html',
            data: {
                commentId: commentId
            },
            success: function (result, status, xhr) {
                // Call Partial View _ReplyCommentText
                $('#comment_' + commentId).html(result);
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            }
        };
        $.ajax(ajaxConfig);
    },
    addNewLikeComment: function (likeComment) {
        var likeCommentId = likeComment["commentId"];
        var ajaxConfig = {
            url: '/LikeComment/AddNewLike',
            type: 'POST',
            dataType: 'JSON',
            data: {
                likeCommentInfo: JSON.stringify(likeComment)
            },
            success: function (result, status, xhr) {
                if (result.status) {
                    var totalLikes = result.totalLikes;
                    if (totalLikes > 1) {
                        $('#small-numberOfCommentLikes_' + likeCommentId).html(totalLikes + ' Likes &nbsp;');
                    } else {
                        $('#small-numberOfCommentLikes_' + likeCommentId).html(totalLikes + ' Like &nbsp;');
                    }
                } else {
                    toastr.error(error);
                }

            },
            error: function (xhr, status, error) {
                toastr.error(error);
            }
        };
        $.ajax(ajaxConfig);
    }
};
commentAreaController.init();