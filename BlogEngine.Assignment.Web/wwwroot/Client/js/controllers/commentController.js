﻿let commentController = {
    init: function () {
        commentController.registerEvents();
        commentController.customRules();
    },
    registerEvents: function () {

        // Show all comments
        $('#numberOfComments').off('click').on('click', function (e) {
            e.preventDefault();
            $('#comment-area').show();
            $('#comment-text').show();
            $('#txtName').focus();
            // Hide reply text
            $('.reply').html('');
        });

        $('#btnShowAllComments').off('click').on('click', function (e) {
            e.preventDefault();
            let isSignedIn = $('#txtIsSignedIn').val();
            if (!isSignedIn || isSignedIn !== 'value') {
                $('#comment-area').show();
                $('#comment-text').show();
                $('#txtName').focus();
                // Hide reply text
                $('.reply').html('');
            } else {
                $('#comment-area').show();
                $('#comment-text').show();
                $('#txtComment').focus();
                // Remove textbox email and name in DOM
                $('#txtEmail').remove();
                $('#txtName').remove();
                $('#txtComment').removeClass('small-space');

                // Hide reply text
                $('.reply').html('');
            }
        });

        $('#comments').off('click').on('click', function (e) {
            e.preventDefault();
            let isSignedIn = $('#txtIsSignedIn').val();
            if (!isSignedIn || isSignedIn !== 'value') {
                $('#comment-area').show();
                $('#comment-text').show();
                $('#txtName').focus();
                // Hide reply text
                $('.reply').html('');
            } else {
                $('#comment-area').show();
                $('#comment-text').show();
                $('#txtComment').focus();
                // Remove textbox email and name in DOM
                $('#txtEmail').remove();
                $('#txtName').remove();
                $('#txtComment').removeClass('small-space');

                // Hide reply text
                $('.reply').html('');
            }
        });

        $('#btnPostComment').off('click').on('click', function (e) {
            e.preventDefault();
            let isSignedIn = $('#txtIsSignedIn').val();
            //if user hasn't signed in yet
            if (!isSignedIn || isSignedIn !== 'value') {
                // Adding new comment is like guests 
                let isValid = $('#frm_comment').valid();
                if (isValid) {
                    let name = $('#txtName').val().trim();
                    let email = $('#txtEmail').val().trim();
                    let comment = $('#txtComment').val().trim();
                    let postId = $('#txtPostId').val().trim();
                    let countPostComments = 0;
                    countPostComments++;

                    let commentObj = {
                        name: name,
                        userName: 'Guest',
                        content: comment,
                        email: email,
                        createdDate: new Date(),
                        createdBy: 'Guest',
                        updatedBy: null,
                        status: true,
                        countPostComments: countPostComments,
                        postID: postId
                    };
                    commentController.addNewComment(commentObj);
                } else {
                    commentController.validatedFields();
                }
            } else {
                let username = $('#a-userName').text().trim();
                if (username !== null) {
                    let postID = $('#txtPostId').val().trim();
                    let userComment = $('#txtComment').val().trim();
                    let countPostComments = 0;
                    countPostComments++;

                    let commentObj = {
                        name: null,
                        userName: username,
                        content: userComment,
                        email: null,
                        createdDate: new Date(),
                        createdBy: username,
                        updatedBy: null,
                        status: true,
                        countPostComments: countPostComments,
                        postID: postID
                    };
                    commentController.addNewComment(commentObj);
                }
            }
        });
        $('#txtComment').off('keyup').on('keyup', function (e) {
            let comment = $('#txtComment').val();
            if (comment.length !== 0) {
                $('#post_comment').show();
            } else {
                $('#post_comment').hide();
            }
            // Comment rule algorithm 
            let ruleObj = commentController.commentRule(comment);
            let pasteKey = e.ctrlKey === true && e.keyCode === 86;
            let copyKey = e.ctrlKey === true && e.keyCode === 67;

            if (e.keyCode !== 17 && pasteKey === true) {
                let actualRows = 2 + ruleObj.newLine;
                $('#txtComment').prop('rows', actualRows);
            } else {                
                let actualRows = 2 + ruleObj.newLine;
                $('#txtComment').prop('rows', actualRows);
            }
            console.log(ruleObj.ruleStart + "<=" + ruleObj.textLength + "<=" + ruleObj.ruleEnd + " ==> Rows:" + (2 + ruleObj.newLine));
        });

        // Show all reply comments
        $('.btnReplyComment').off('click').on('click', function (e) {
            e.preventDefault();
            let isSignedIn = $('#txtIsSignedIn').val();
            let commentId = $(this).attr('id').split('_')[1];

            $('.reply').html('');
            $('#comment-text').hide();
            commentController.getAllReplyComments(commentId, isSignedIn);
        });

        // Show all reply comments
        $('.small-numberOfReplyComments').off('click').on('click', function (e) {
            e.preventDefault();
            let commentId = $(this).attr('id').split('_')[1];
            let isSignedIn = $('#txtIsSignedIn').val();

            $('.reply').html('');
            $('#comment-text').hide();
            commentController.getAllReplyComments(commentId, isSignedIn);
        });
        let frm_commentConfig = {
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 255,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                email: {
                    required: true,
                    email: true,
                    minlength: 12,
                    maxlength: 50,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                }
            },
            messages: {
                email: {
                    required: 'Email is required',
                    email: 'Email is invalid',
                    minlength: 'Email requires at least 12 characters',
                    maxlenght: 'Email cannot length over 50 characters',
                    preventScriptInjection: 'Email cannot contain any javascript code',
                    preventHtmlInjection: 'Email cannot contain any html characters',
                    preventWhiteSpace: 'Email cannot contain any white space or empty string'
                },
                name: {
                    required: 'Name is required',
                    minlength: 'Name requires at least 2 characters',
                    maxlength: 'Name name cannot length over 255 characters',
                    preventScriptInjection: 'Name cannot contain any javascript code',
                    preventHtmlInjection: 'Name cannot contain any html characters',
                    preventWhiteSpace: 'Name cannot contain any white space or empty string'
                }
            }
        };

        $('#txtName').off('keyup').on('keyup', function (event) {
            let nameValid = $('#txtName').valid();
            switch (nameValid) {
                case true:
                    $('#txtName').removeClass('input-fields-textbox-error');
                    $('#txtName-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtName').addClass('input-fields-textbox-error');
                    $('#txtName-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#txtEmail').off('keyup').on('keyup', function (event) {
            let emailValid = $('#txtEmail').valid();
            switch (emailValid) {
                case true:
                    $('#txtEmail').removeClass('input-fields-textbox-error');
                    $('#txtEmail-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtEmail').addClass('input-fields-textbox-error');
                    $('#txtEmail-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#frm_comment').validate(frm_commentConfig);
    },
    commentRule: function (comment) {
        let start = 0;
        let end = 81;
        let rows = 0;
        let ruleObj = {
            ruleStart: 0,
            textLength: 0,
            ruleEnd: 81,
            newLine: 0
        };
        let textLength = comment.length;
        if (textLength <= end) return ruleObj;
        while (ruleObj.ruleStart < end) {
            if (textLength < ruleObj.ruleEnd) return ruleObj;
            else {
                rows++;
                start = start + 82;
                end = start + 81;
                if (start <= textLength && textLength <= end) {
                    ruleObj = {
                        ruleStart: start,
                        textLength: textLength,
                        ruleEnd: end,
                        newLine: rows
                    };
                    return ruleObj;
                }
            }
        }
        return ruleObj;
    },
    addNewComment: function (comment) {
        let ajaxConfig = {
            url: '/Comment/AddNewComment',
            type: 'POST',
            dataType: 'html',
            data: {
                commentInfo: JSON.stringify(comment)
            },
            beforeSend: function () {
                $('#wait').show();
            },
            success: function (response, status, xhr) {
                if (response) {
                    // Hide comment textbox
                    $('#comment-text').hide();
                    // Hide comment button
                    $('#post_comment').hide();

                    // Set default value for text box
                    $('#txtComment').val('');
                    $('#txtName').val('');
                    $('#txtEmail').val('');

                    // After add new comment on blog/article
                    // Call Partial View _CommentArea
                    $('#comment-area').html(response);
                    let postId = $('#txtPostId').val();
                    // Update post comments and likes
                    commentController.updatePostComments(postId);
                }
            }, error: function (xhr, status, error) {
                toastr.error(error, 'Error');
            },
            complete: function (xhr, textStatus) {
                // A function to run when the request is finished (after success and error functions)
                $('#wait').hide();
            }
        };
        $.ajax(ajaxConfig);
    },
    getAllReplyComments: function (commentId, isSignedIn) {
        let ajaxConfig = {
            url: '/ReplyComment/GetAllReplyComments',
            type: 'POST',
            dataType: 'html',
            data: {
                commentId: commentId
            },
            beforeSend: function () {
                $('#wait').show();
            },
            success: function (result, status, xhr) {
                // Call Partial View _ReplyCommentText
                if (!isSignedIn) {
                    $('#comment_' + commentId).html(result);
                    let userName = $('#lblCommentUserName_' + commentId).text().trim();
                    $('#txtReplyComment').val('@' + userName);
                } else {
                    $('#comment_' + commentId).html(result);
                    $('#txtReplyName').remove();
                    $('#txtReplyEmail').remove();
                    $('#txtReplyComment').removeClass('small-space');

                    let userName = $('#lblCommentUserName_' + commentId).text().trim();
                    $('#txtReplyComment').val('@' + userName);
                }
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            },
            complete: function (xhr, textStatus) {
                // A function to run when the request is finished (after success and error functions)
                $('#wait').hide();
            }
        };
        $.ajax(ajaxConfig);
    },
    updatePostComments: function (postId) {
        let ajaxConfig = {
            url: '/Comment/UpdatePostComments',
            type: 'POST',
            dataType: 'JSON',
            data: {
                postId: postId
            },
            beforeSend: function () {
                $('#wait').show();
            },
            success: function (response, status, xhr) {
                if (response.status) {
                    let postViewModel = JSON.parse(response.postVm);
                    let totalComments = postViewModel.countComments;
                    if (totalComments > 1) {
                        // Update total comments on UI
                        $('#numberOfComments').text(totalComments + ' comments');
                        $('#small-numberOfComments').text(totalComments + ' comments');
                    }
                    else {
                        // Update total comments on UI
                        $('#numberOfComments').text(totalComments + ' comment');
                        $('#small-numberOfComments').text(totalComments + ' comment');
                    }
                } else {
                    toastr.error(response.error);
                }
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            },
            complete: function (xhr, textStatus) {
                // A function to run when the request is finished (after success and error functions)
                $('#wait').hide();
            }
        };
        $.ajax(ajaxConfig);
    },
    validatedFields: function () {
        let nameValid = $('#txtName').valid();
        let emailValid = $('#txtEmail').valid();
        let replyCommentValid = $('#txtComment').valid();

        switch (nameValid) {
            case true:
                $('#txtName').removeClass('input-fields-textbox-error');
                $('#txtName-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtName').addClass('input-fields-textbox-error');
                $('#txtName-error').addClass('input-fields-label-error');
                break;
        }
        switch (emailValid) {
            case true:
                $('#txtEmail').removeClass('input-fields-textbox-error');
                $('#txtEmail-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtEmail').addClass('input-fields-textbox-error');
                $('#txtEmail-error').addClass('input-fields-label-error');
                break;
        }
        switch (replyCommentValid) {
            case true:
                $('#txtComment').removeClass('input-fields-textbox-error');
                $('#txtComment-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtComment').addClass('input-fields-textbox-error');
                $('#txtComment-error').addClass('input-fields-label-error');
                break;
        }
    },
    customRules: function () {
        jQuery.validator.addMethod('preventScriptInjection', function (value, element, params) {
            if (value.toLowerCase().search('<script>') !== -1 || value.toLowerCase().search('</script>') !== -1)
                return false; // has script
            return true; // none script
        }, '');
        jQuery.validator.addMethod('preventHtmlInjection', function (value, element, params) {
            let htmlPattern = new RegExp(/<[a-z][\s\S]*>/i);
            if (htmlPattern.test(value))
                return false; // has html
            return true; // none html
        }, '');
        jQuery.validator.addMethod('preventWhiteSpace', function (value, element, params) {
            if (value.trim() === '')
                return false; // has empty string
            return true; // none empty string
        }, '');
    }
};
commentController.init();