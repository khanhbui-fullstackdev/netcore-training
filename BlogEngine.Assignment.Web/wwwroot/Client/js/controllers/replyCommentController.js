﻿let replyCommentController = {
    init: function () {
        replyCommentController.registerEvents();
        replyCommentController.customRules();
        replyCommentController.setDefaultValueForReplyCommentText();
    },
    registerEvents: function () {
        // Reply Comment rule
        $('#txtReplyComment').off('keyup').on('keyup', function (e) {
            let replyComment = $('#txtReplyComment').val();
            if (replyComment.length !== 0) {
                // Show the button area when it meets condition
                $('#reply-comment').show();
            } else {
                // Hide the button area when it meets condition
                $('#reply-comment').hide();
            }
            // Reply Comment rule algorithm 
            let ruleObj = replyCommentController.commentRule(replyComment);
            let pasteKey = e.ctrlKey === true && e.keyCode === 86;
            let copyKey = e.ctrlKey === true && e.keyCode === 67;

            if (e.keyCode !== 17 && pasteKey === true) {
                let actualRows = 2 + ruleObj.newLine;
                $('#txtReplyComment').prop('rows', actualRows);
            } else {
                let actualRows = 2 + ruleObj.newLine;
                $('#txtReplyComment').prop('rows', actualRows);
            }
            console.log(ruleObj.ruleStart + "<=" + ruleObj.textLength + "<=" + ruleObj.ruleEnd + " ==> Rows:" + (2 + ruleObj.newLine));
        });

        let frm_replyCommentConfig = {
            rules: {
                replyName: {
                    required: true,
                    minlength: 2,
                    maxlength: 255,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                replyEmail: {
                    required: true,
                    email: true,
                    minlength: 12,
                    maxlength: 50,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                replyComment: {
                    required: true
                }
            },
            messages: {
                replyEmail: {
                    required: 'Email is required',
                    email: 'Email is invalid',
                    minlength: 'Email requires at least 12 characters',
                    maxlenght: 'Email cannot length over 50 characters',
                    preventScriptInjection: 'Email cannot contain any javascript code',
                    preventHtmlInjection: 'Email cannot contain any html characters',
                    preventWhiteSpace: 'Email cannot contain any white space or empty string'
                },
                replyName: {
                    required: 'Name is required',
                    minlength: 'Name requires at least 2 characters',
                    maxlength: 'Name name cannot length over 255 characters',
                    preventScriptInjection: 'Name cannot contain any javascript code',
                    preventHtmlInjection: 'Name cannot contain any html characters',
                    preventWhiteSpace: 'Name cannot contain any white space or empty string'
                },
                replyComment: {
                    required: 'Comment is required'
                }
            }
        };
        $('#txtReplyName').off('keyup').on('keyup', function (event) {
            let nameValid = $('#txtReplyName').valid();
            switch (nameValid) {
                case true:
                    $('#txtReplyName').removeClass('input-fields-textbox-error');
                    $('#txtReplyName-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtReplyName').addClass('input-fields-textbox-error');
                    $('#txtReplyName-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#txtReplyEmail').off('keyup').on('keyup', function (event) {
            let emailValid = $('#txtReplyEmail').valid();
            switch (emailValid) {
                case true:
                    $('#txtReplyEmail').removeClass('input-fields-textbox-error');
                    $('#txtReplyEmail-error').removeClass('input-fields-label-error');
                    break;
                case false:
                    $('#txtReplyEmail').addClass('input-fields-textbox-error');
                    $('#txtReplyEmail-error').addClass('input-fields-label-error');
                    break;
            }
        });
        $('#frm_replyComment').validate(frm_replyCommentConfig);

        // Post a comment
        $('#btnPostReplyComment').off('click').on('click', function (e) {
            e.preventDefault();
            replyCommentController.saveChanges();
        });
        // Reply person comment
        $('.btnReplyPersonComment').off('click').on('click', function (e) {
            e.preventDefault();
            let replyCommentId = $(this).attr('id').split('_')[1];
            let replyCommentName = $('#lblReplyCommentUserName_' + replyCommentId).text();
            console.log(replyCommentId);
            $('#frm_replyComment').show();
            $('#txtReplyName').focus();
            $('#txtReplyComment').val('@' + replyCommentName);
        });
    },
    addNewReplyComment: function (replyCommentInfo) {
        let commentId = replyCommentInfo['commentId'];
        let ajaxConfig = {
            url: '/ReplyComment/AddNewReplyComment',
            type: 'POST',
            dataType: 'html',
            data: {
                replyCommentInfo: JSON.stringify(replyCommentInfo)
            },
            beforeSend: function () {
                $('#wait').show();
            },
            success: function (response, status, xhr) {
                response = response.trim();
                $.when($('#replyComment-area').html(response))
                    .then(replyCommentController.updateReplyComments(commentId));
                // Hide Reply Comment Text section
                $('#frm_replyComment').hide();
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            },
            complete: function (xhr, textStatus) {
                // A function to run when the request is finished (after success and error functions)
                $('#wait').hide();
            }
        };
        $.ajax(ajaxConfig);
    },
    updateReplyComments: function (commentId) {
        let ajaxConfig = {
            url: '/ReplyComment/UpdateReplyComments',
            type: 'POST',
            dataType: 'JSON',
            data: {
                commentId: commentId
            },
            beforeSend: function () {
                $('#wait').show();
            },
            success: function (response, status, xhr) {
                if (response.status) {
                    let commentViewModel = JSON.parse(response.commentVm);
                    let countReplyComments = commentViewModel.countReplyComments;
                    if (countReplyComments > 1) {
                        // Update reply comments on UI
                        $('#small-numberOfReplyComments_' + commentId).text(countReplyComments + ' Replies');
                    } else {
                        // Update reply comments on UI
                        $('#small-numberOfReplyComments_' + commentId).text(countReplyComments + ' Reply');
                    }
                } else {
                    toastr.error(response.error, 'Error');
                }
            },
            error: function (xhr, status, error) {
                toastr.error(error, 'Error');
            },
            complete: function (xhr, textStatus) {
                // A function to run when the request is finished (after success and error functions)
                $('#wait').hide();
            }
        };
        $.ajax(ajaxConfig);
    },
    saveChanges: function () {
        let isSignedIn = $('#txtIsSignedIn').val();
        if (!isSignedIn || isSignedIn !== 'value') {
            let isValid = $('#frm_replyComment').valid();
            if (isValid) {
                let replyName = $('#txtReplyName').val();
                let replyEmail = $('#txtReplyEmail').val();
                let replyComment = $('#txtReplyComment').val();
                let commentGuidId = $('#txtCommentGuidId').val();
                let countReplyComments = 0;
                countReplyComments++;

                let replyCommentObj = {
                    name: replyName,
                    userName: 'Guest',
                    email: replyEmail,
                    content: replyComment,
                    createdDate: new Date(),
                    createdBy: 'Guest',
                    updateBy: null,
                    status: true,
                    countReplyComments: countReplyComments,
                    commentId: commentGuidId
                };
                replyCommentController.addNewReplyComment(replyCommentObj);
            } else {
                replyCommentController.validatedFields();
            }
        } else {
            let username = $('#a-userName').text().trim();
            if (username !== null) {
                let replyComment = $('#txtReplyComment').val();
                let commentGuidId = $('#txtCommentGuidId').val();
                let countReplyComments = 0;
                countReplyComments++;

                let replyCommentObj = {
                    name: null,
                    userName: username,
                    email: null,
                    content: replyComment,
                    createdDate: new Date(),
                    createdBy: username,
                    updateBy: null,
                    status: true,
                    countReplyComments: countReplyComments,
                    commentId: commentGuidId
                };
                replyCommentController.addNewReplyComment(replyCommentObj);
            }
        }
    },
    addNewLike: function () {

    },
    setDefaultValueForReplyCommentText: function () {
        $('#txtReplyName').val('');
        $('#txtReplyEmail').val('');
        $('#txtReplyComment').val('');

        $('#txtReplyName').focus();
    },
    validatedFields: function () {
        let nameValid = $('#txtReplyName').valid();
        let emailValid = $('#txtReplyEmail').valid();
        let replyCommentValid = $('#txtReplyComment').valid();

        switch (nameValid) {
            case true:
                $('#txtReplyName').removeClass('input-fields-textbox-error');
                $('#txtReplyName-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtReplyName').addClass('input-fields-textbox-error');
                $('#txtReplyName-error').addClass('input-fields-label-error');
                break;
        }
        switch (emailValid) {
            case true:
                $('#txtReplyEmail').removeClass('input-fields-textbox-error');
                $('#txtReplyEmail-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtReplyEmail').addClass('input-fields-textbox-error');
                $('#txtReplyEmail-error').addClass('input-fields-label-error');
                break;
        }
        switch (replyCommentValid) {
            case true:
                $('#txtReplyComment').removeClass('input-fields-textbox-error');
                $('#txtReplyComment-error').removeClass('input-fields-label-error');
                break;
            case false:
                $('#txtReplyComment').addClass('input-fields-textbox-error');
                $('#txtReplyComment-error').addClass('input-fields-label-error');
                break;
        }
    },
    commentRule: function (comment) {
        let start = 0;
        let end = 69;
        let rows = 0;
        let ruleObj = {
            ruleStart: 0,
            textLength: 0,
            ruleEnd: 69,
            newLine: 0
        };
        let textLength = comment.length;
        if (textLength <= end) return ruleObj;
        while (ruleObj.ruleStart < end) {
            if (textLength < ruleObj.ruleEnd) return ruleObj;
            else {
                rows++;
                start = start + 70;
                end = start + 69;
                if (start <= textLength && textLength <= end) {
                    ruleObj = {
                        ruleStart: start,
                        textLength: textLength,
                        ruleEnd: end,
                        newLine: rows
                    };
                    return ruleObj;
                }
            }
        }
        return ruleObj;
    },
    customRules: function () {
        jQuery.validator.addMethod('preventScriptInjection', function (value, element, params) {
            if (value.toLowerCase().search('<script>') !== -1 || value.toLowerCase().search('</script>') !== -1)
                return false; // has script
            return true; // none script
        }, '');
        jQuery.validator.addMethod('preventHtmlInjection', function (value, element, params) {
            let htmlPattern = new RegExp(/<[a-z][\s\S]*>/i);
            if (htmlPattern.test(value))
                return false; // has html
            return true; // none html
        }, '');
        jQuery.validator.addMethod('preventWhiteSpace', function (value, element, params) {
            if (value.trim() === '')
                return false; // has empty string
            return true; // none empty string
        }, '');
    }
};
replyCommentController.init();