﻿var likePostController = {
    init: function () {
        likePostController.registerEvents();
    },
    registerEvents: function () {
        $('#likes').off('click').on('click', function (e) {
            e.preventDefault();
            var isSignedIn = $('#txtIsSignedIn').val();
            if (!isSignedIn || isSignedIn !== 'value')
                commonController.signIn();
            else {
                var userName = $('#a-userName').text().trim();
                var postId = $('#txtPostId').val();
                var countLikesPost = 0;
                countLikesPost++;

                var likePost = {
                    userName: userName,
                    postId: postId,
                    countLikesPost: countLikesPost
                };
                console.log(likePost);
                likePostController.addNewLike(likePost);
            }
        });
    },  
    addNewLike: function (likePost) {
        var ajaxConfig = {
            url: '/LikePost/AddNewLike',
            type: 'POST',
            dataType: 'JSON',
            data: {
                likePostInfo: JSON.stringify(likePost)
            },
            beforeSend: function () {
                $('#wait').show();
            },
            success: function (response, status, xhr) {
                if (response.status) {
                    var totalLikes = response.totalLikes;
                    if (totalLikes > 1) {
                        $('#small-numberOfLikes').text(totalLikes + ' likes');
                    } else {
                        $('#small-numberOfLikes').text(totalLikes + ' like');
                    }
                } else {
                    toastr.error(response.error);
                }
            },            
            error: function (xhr, status, error) {
                toastr.error(error);
            },
            complete: function (xhr, textStatus) {
                // A function to run when the request is finished (after success and error functions)
                $('#wait').hide();
            }
        };
        $.ajax(ajaxConfig);
    }
};
likePostController.init();
