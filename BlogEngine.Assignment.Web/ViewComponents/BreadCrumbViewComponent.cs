﻿using BlogEngine.Assignment.Common.Constants;
using BlogEngine.Assignment.Service.IServices;
using BlogEngine.Assignment.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Web.ViewComponents
{
    public class BreadCrumbViewComponent : ViewComponent
    {
        private readonly ICategoryService _categoryService;
        private readonly ISubCategoryService _subCategoryService;

        public BreadCrumbViewComponent(
            ICategoryService categoryService,
            ISubCategoryService subCategoryService)
        {
            this._categoryService = categoryService;
            this._subCategoryService = subCategoryService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? categoryId, int? subcategoryId)
        {
            BreadCrumbViewModel breadCrumbViewModel = null;
            if (categoryId.HasValue)
            {
                var category = await _categoryService.GetCategoryById(categoryId.Value);
                breadCrumbViewModel = new BreadCrumbViewModel
                {
                    CategoryId = category.ID,
                    Name = category.Name
                };
                return View(ViewUrl._BreadCrumb, breadCrumbViewModel);
            }
            else if (subcategoryId.HasValue)
            {
                var subcategory = await _subCategoryService.GetSubCategoryById(subcategoryId.Value);

                breadCrumbViewModel = new BreadCrumbViewModel()
                {
                    CategoryId = subcategory.CategoryID,
                    SubCategoryId = subcategory.ID,
                    Name = subcategory.Category.Name + "/" + subcategory.Name
                };
                return View(ViewUrl._BreadCrumb, breadCrumbViewModel);
            }
            return View(ViewUrl._BreadCrumb, breadCrumbViewModel);
        }
    }
}
