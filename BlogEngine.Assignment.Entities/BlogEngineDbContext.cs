﻿using BlogEngine.Assignment.Entities.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace BlogEngine.Assignment.Entities
{
    public class BlogEngineDbContext : IdentityDbContext<BloggerUser, BloggerRole, string>
    {
        public BlogEngineDbContext(DbContextOptions<BlogEngineDbContext> options) : base(options)
        {

        }

        #region Entities Set
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostTag> PostTags { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<ReplyComment> ReplyComments { get; set; }
        public DbSet<LikeComment> LikeComments { get; set; }
        public DbSet<LikePost> LikePosts { get; set; }

        // ASP.NET Identity
        public DbSet<BloggerUser> BloggerUsers { set; get; }
        public DbSet<BloggerGroup> BloggerGroups { set; get; }
        public DbSet<BloggerRole> BloggerRoles { set; get; }
        public DbSet<BloggerRoleGroup> BloggerRoleGroups { set; get; }
        public DbSet<BloggerUserGroup> BloggerUserGroups { set; get; }
        public DbSet<BloggerUserRole> BloggerUserRoles { set; get; }
        #endregion

        #region On Configuring
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = @"Data Source=localhost;Initial Catalog=BlogEngineDotNetCore;User Id=sa;Password=P@ssword123;MultipleActiveResultSets=true";
            optionsBuilder.UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60));
        }
        #endregion

        #region On Model Creating
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            ConfigureIdentiyTables(modelBuilder);
            Seed(modelBuilder);
        }
        #endregion

        #region Configure Identity Tables
        private void ConfigureIdentiyTables(ModelBuilder modelBuilder)
        {
            // Config IdentityUser
            modelBuilder.Entity<BloggerUser>(buildAction =>
            {
                buildAction.HasKey(u => u.Id); // Define primary key
                buildAction.Property(u => u.Id).HasColumnName("BloggerUserId").HasColumnType("nvarchar(450)").HasMaxLength(450);

                // Indexes for "normalized" username and email, to allow efficient lookups
                buildAction.HasIndex(u => u.NormalizedUserName).HasName("UserNameIndex").IsUnique();
                buildAction.HasIndex(u => u.NormalizedEmail).HasName("EmailIndex");

                // Limit the size of columns to use efficient database types
                buildAction.Property(u => u.UserName).HasMaxLength(255).HasColumnType("varchar(255)");
                buildAction.Property(u => u.NormalizedUserName).HasMaxLength(255).HasColumnType("varchar(255)");
                buildAction.Property(u => u.Email).HasMaxLength(255).HasColumnType("varchar(255)");
                buildAction.Property(u => u.NormalizedEmail).HasMaxLength(255).HasColumnType("varchar(255)");

                buildAction.ToTable("BloggerUsers", "dbo"); // Maps to BloggerUsers table
            });

            // Config IdentityUserRole
            modelBuilder.Entity<IdentityUserRole<string>>(buildAction =>
            {
                buildAction.HasKey(x => new { x.UserId, x.RoleId });
                buildAction.Property(p => p.UserId).HasColumnName("BloggerUserId").HasMaxLength(450).HasColumnType("nvarchar(450)");
                buildAction.Property(p => p.RoleId).HasColumnName("BloggerRoleId").HasMaxLength(450).HasColumnType("nvarchar(450)");
                buildAction.ToTable("BloggerUserRoles", "dbo"); // Maps to BloggerUserRoles table
            });

            // Config IdentityRole
            modelBuilder.Entity<BloggerRole>(buildAction =>
            {
                buildAction.HasKey(p => p.Id);
                buildAction.Property(p => p.Id).HasColumnName("BloggerRoleId").HasMaxLength(450).HasColumnType("nvarchar(450)");
                buildAction.Property(x => x.Name).HasColumnName("RoleName").HasColumnType("nvarchar(255)").HasMaxLength(255).IsRequired();

                // Limit the size of columns to use efficient database types
                buildAction.Property(u => u.NormalizedName).HasMaxLength(255);
                buildAction.Property(u => u.ConcurrencyStamp).HasMaxLength(255).HasColumnType("varchar(255)");
                buildAction.ToTable("BloggerRoles", "dbo");
            });

            // Config IdentityUserLogin
            modelBuilder.Entity<IdentityUserLogin<string>>(buildAction =>
            {
                buildAction.HasKey(p => p.UserId);
                buildAction.Property(p => p.UserId).HasColumnName("BloggerUserId").HasMaxLength(450).HasColumnType("nvarchar(450)");
                buildAction.ToTable("BloggerUserLogins", "dbo");
            });

            // Config IdentityUserClaim
            modelBuilder.Entity<IdentityUserClaim<string>>(buildAction =>
            {
                buildAction.HasKey(x => x.UserId);
                buildAction.Property(x => x.UserId).HasColumnName("BloggerUserId").HasMaxLength(450).HasColumnType("nvarchar(450)");
                buildAction.ToTable("BloggerUserClaims", "dbo");
            });

            // Config IdentityRoleClaim
            modelBuilder.Entity<IdentityRoleClaim<string>>(buildAction =>
            {
                buildAction.HasKey(x => x.RoleId);
                buildAction.Property(x => x.RoleId).HasColumnName("BloggerRoleId").HasMaxLength(450).HasColumnType("nvarchar(450)");

                // Limit the size of columns to use efficient database types
                buildAction.Property(u => u.ClaimType).HasMaxLength(255).HasColumnType("varchar(255)");
                buildAction.Property(u => u.ClaimValue).HasMaxLength(500).HasColumnType("nvarchar(500)");
                buildAction.ToTable("BloggerRoleClaims", "dbo");
            });

            //Config IdentityUserToken
            modelBuilder.Entity<IdentityUserToken<string>>(buildAction =>
            {
                buildAction.HasKey(x => x.UserId); // Define primary key
                buildAction.Property(x => x.UserId).HasColumnName("BloggerUserId").HasMaxLength(450).HasColumnType("nvarchar(450)"); // change primarykey name into BloggerUserId

                // Limit the size of columns to use efficient database types
                buildAction.Property(u => u.LoginProvider).HasMaxLength(255);
                buildAction.Property(u => u.Value).HasMaxLength(500).HasColumnType("nvarchar(500)");
                buildAction.Property(u => u.Name).HasMaxLength(255).HasColumnType("varchar(255)");
                buildAction.ToTable("BloggerUserTokens", "dbo");
            });
        }
        #endregion

        #region Seed
        private void Seed(ModelBuilder modelBuilder)
        {
            SeedCategoryData(modelBuilder);
            SeedSubcategoryData(modelBuilder);
            SeedPostData(modelBuilder);
        }
        #endregion

        #region Seed Category Data
        private void SeedCategoryData(ModelBuilder modelBuilder)
        {
            modelBuilder
               .Entity<Category>()
               .HasData(
                    new Category()
                    {
                        ID = 1,
                        Name = "C# Programming",
                        Slug = "csharp",
                        Image = "",
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                new Category()
                {
                    ID = 2,
                    Name = "ASP.NET Core",
                    Slug = "asp.net-core",
                    Image = "",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true
                },
                new Category()
                {
                    ID = 3,
                    Name = "ASP.NET Web Application",
                    Slug = "asp.net-web-application",
                    Image = "",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true
                },
                new Category()
                {
                    ID = 4,
                    Name = "Windows Desktop Application",
                    Slug = "windows-desktop-application",
                    Image = "",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true
                },
                new Category()
                {
                    ID = 5,
                    Name = "HTML, CSS & Boostrap",
                    Slug = "html-css-boostrap",
                    Image = "",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true
                },
                new Category()
                {
                    ID = 6,
                    Name = "Javascript Frameworks",
                    Slug = "javascript-framework",
                    Image = "",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true
                },
                new Category()
                {
                    ID = 7,
                    Name = "Entity FrameWork 6/Entity FrameWork Core",
                    Slug = "entity-framework",
                    Image = "",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true
                },
                new Category()
                {
                    ID = 8,
                    Name = "Database",
                    Slug = "database",
                    Image = "",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true
                },
                new Category()
                {
                    ID = 9,
                    Name = "Security",
                    Slug = "security",
                    Image = "",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true
                });
        }
        #endregion

        #region Seed Subcategory Data
        private void SeedSubcategoryData(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<SubCategory>()
                .HasData(
                    new SubCategory()
                    {
                        ID = 1,
                        Name = "ASP.NET MVC5",
                        Slug = "asp.net-mvc5",
                        CategoryID = 3,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 2,
                        Name = "ASP.NET MVC Core",
                        Slug = "asp.net-mvc-core",
                        CategoryID = 2,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 3,
                        Name = "ASP.NET Web Form",
                        Slug = "asp.net-web-form",
                        CategoryID = 3,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 4,
                        Name = "NET Windows Form",
                        Slug = "winform",
                        CategoryID = 4,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 5,
                        Name = "Windows Presentation Foundation",
                        Slug = "winform-presentation-foundataion",
                        CategoryID = 4,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 6,
                        Name = "JQuery",
                        Slug = "jquery",
                        CategoryID = 6,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 7,
                        Name = "KnockoutJS",
                        Slug = "knockoutjs",
                        CategoryID = 6,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 8,
                        Name = "Angularjs",
                        Slug = "angularjs",
                        CategoryID = 6,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 9,
                        Name = "Angular 5",
                        Slug = "angular5",
                        CategoryID = 6,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 10,
                        Name = "Reactjs",
                        Slug = "Reactjs",
                        CategoryID = 6,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 11,
                        Name = "Vuejs",
                        Slug = "vuejs",
                        CategoryID = 6,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 12,
                        Name = "My SQL",
                        Slug = "mysql",
                        CategoryID = 8,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 13,
                        Name = "Microsoft SQL Server",
                        Slug = "microsoft-sql-server",
                        CategoryID = 8,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 14,
                        Name = "Oracle",
                        Slug = "oracle",
                        CategoryID = 8,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    },
                    new SubCategory()
                    {
                        ID = 15,
                        Name = "Object Oriented Programming",
                        Slug = "oop",
                        CategoryID = 1,
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true
                    });
        }
        #endregion

        #region Seed Post Data
        private void SeedPostData(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Post>()
                .HasData(
                    new Post()
                    {
                        ID = 1,
                        Name = "Introducing Nullable Reference Types in C#",
                        Slug = "introducing-nullable-reference-types-in-csharp",
                        CategoryID = 1,
                        Summary = "Today we released a prototype of a C# feature called “nullable reference types“, which is intended to help you find and fix most of your null-related bugs before they blow up at runtime. We would love for you to install the prototype and try it out on your code! (Or maybe a copy of it!",
                        CreatedDate = DateTime.Now,
                        CreatedBy = "khanh.buivuong@nashtechglobal.com",
                        Status = true,
                        Quote = "Design is not just what is look like, Design is how it's work.",
                        Content = @"
Today we released a prototype of a C# feature called “nullable reference types“, which is intended to help you find and fix most of your null-related bugs before they blow up at runtime.
We would love for you to install the prototype and try it out on your code! (Or maybe a copy of it! 😄) Your feedback is going to help us get the feature exactly right before we officially release it.
The billion-dollar mistake Tony Hoare, one of the absolute giants of computer science and recipient of the Turing Award, invented the null reference! It’s crazy these days to think that something as foundational and ubiquitous was invented, but there it is. Many years later in a talk, Sir Tony actually apologized, calling it his “billion-dollar mistake”:".Trim(),
                        Image = "/Client/images/projects/project-5.png"
                    },

                new Post()
                {
                    ID = 2,
                    Name = ".NET Core 2.1 June Update",
                    Slug = "net-core-2-1-june-update",
                    CategoryID = 2,
                    Summary = @"We released .NET Core 2.1.1. This update includes .NET Core SDK 2.1.301, ASP.NET Core 2.1.1 and .NET Core 2.1.1. See .NET Core 2.1.1 release notes for complete details on the release. Quality Updates CLI [4050c6374] The “pack” command under ‘buildCrossTargeting’ for ‘Microsoft.DotNet.MSBuildSdkResolver’ now throws a “NU5104” warning/error because the SDK stage0 was changed to “2.1.300” [change was",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "Because of your smile, you make life more beautiful",
                    Content = @"CLI
                                [4050c6374] The “pack” command under ‘buildCrossTargeting’ for ‘Microsoft.DotNet.MSBuildSdkResolver’ now throws a “NU5104” warning/error because the SDK stage0 was changed to “2.1.300” [change was intended].
                                [ea539c7f6] Add retry when Directory.Move (#9313)
                                CoreCLR
                                [13ea3c2c8e] Fix alternate stack for Alpine docker on SELinux (#17936) (#17975)
                                [88db627a97] Update g_highest_address and g_lowest_address in StompWriteBarrier(WriteBarrierOp::StompResize) on ARM (#18107)
                                [0ea5fc4456] Use sysconf(_SC_NPROCESSORS_CONF) instead of sysconf(_SC_NPROCESSORS_ONLN) in PAL and GC on ARM and ARM64
                                CoreFX
                                [3700c5b793] Update to a xUnit Performance Api that has a bigger Etw buffer size. … (#30328)
                                [6b38470265] Use _SC_NPROCESSORS_CONF instead of _SC_NPROCESSORS_ONLN in Unix_ProcessorCountTest on ARM/ARM64 (#30132)
                                [fe653a068c] check SwitchingProtocol before ContentLength (#29948) (#29993)
                                [f11f3e1fcf] Fix handling of cursor position when other ESC sequences already in stdin (#29897) (#29923)
                                [77a4a19622] [release/2.1] Port nano test fixes (#29995)
                                [7ce9270ac7] Fix Sockets hang caused by concurrent Socket disposal (#29786) (#29846)
                                [ed23f5391f] Fix terminfo number reading with 32-bit integers (#29655) (#29765)
                                [1c34018f14] Fix getting attributes for sharing violation files (#29790) (#29832)
                                [bc71849976] [release/2.1] Fix deadlock when waiting for process exit in Console.CancelKeyPress (#29749)
                                [adc1c4d0d5] Fix WebSocket split UTF8 read #29834 (#29840) (#29853)
                                WCF
                                [0a99dd88] Add net461 as a supported framework for S.SM.Security.
                                [45855085] Generate ThisAssembly.cs, update the version and links for svcutil.xmlserializer (#2893)
                                [68457365] Target svcutil.xmlserializer app at dotnetcore. (#2855)".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 3,
                    Name = "JQuery 3.3.1 – Fixed Dependencies In Release Tag",
                    Slug = "jquery-3-3-1–fixed-dependencies-in-release-tag",
                    CategoryID = 6,
                    Summary = @"We encountered an issue in the release for jQuery 3.3.0, so we’ve immediately released another tag. The code itself is identical, but our release dependencies (only used during release) were added to the dependencies of the jQuery package itself due to the new behavior of npm in version 5+.",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "Jquery is always awesome ! Cheer",
                    Content = @"We encountered an issue in the release for jQuery 3.3.0, so we’ve immediately released another tag. The code itself is identical, but our release dependencies (only used during release) were added to the dependencies of the jQuery package itself due to the new behavior of npm in version 5+.
                                jQuery 3.3.1 is now recommended if installing from npm or GitHub. If using jQuery on any CDN, the built file only differs in the version number.
                                We apologize for any inconvenience and have updated our release script to account for this issue.
                                Please see the jQuery 3.3.0 blog post for all relevant code changes in this release.
                                Download
                                You can get the files from the jQuery CDN, or link to them directly:

                                https://code.jquery.com/jquery-3.3.1.js

                                https://code.jquery.com/jquery-3.3.1.min.js

                                You can also get this release from npm:

                                npm install jquery@3.3.1

                                Slim build
                                Sometimes you don’t need ajax, or you prefer to use one of the many standalone libraries that focus on ajax requests. And often it is simpler to use a combination of CSS and class manipulation for all your web animations. Along with the regular version of jQuery that includes the ajax and effects modules, we’ve released a “slim” version that excludes these modules. The size of jQuery is very rarely a load performance concern these days, but the slim build is about 6k gzipped bytes smaller than the regular version – 24k vs 30k. These files are also available in the npm package and on the CDN:

                                https://code.jquery.com/jquery-3.3.1.slim.js
                                https://code.jquery.com/jquery-3.3.1.slim.min.js

                                These updates are already available as the current versions on npm and Bower. Information on all the ways to get jQuery is available at https://jquery.com/download/. Public CDNs receive their copies today, please give them a few days to post the files. If you’re anxious to get a quick start, use the files on our CDN until they have a chance to update.

                                8 THOUGHTS ON “JQUERY 3.3.1 – FIXED DEPENDENCIES IN RELEASE TAG”".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 4,
                    Name = "Welcome to C# 7.2 and Span 1",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 5,
                    Name = "Welcome to C# 7.2 and Span 2",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 6,
                    Name = "Welcome to C# 7.2 and Span 3",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 7,
                    Name = "Welcome to C# 7.2 and Span 4",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 8,
                    Name = "Welcome to C# 7.2 and Span 5",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 9,
                    Name = "Welcome to C# 7.2 and Span 6",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 10,
                    Name = "Welcome to C# 7.2 and Span 7",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 11,
                    Name = "Welcome to C# 7.2 and Span 8",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 12,
                    Name = "Welcome to C# 7.2 and Span 9",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 13,
                    Name = "Welcome to C# 7.2 and Span 10",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 14,
                    Name = "Welcome to C# 7.2 and Span 11",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 15,
                    Name = "Welcome to C# 7.2 and Span 12",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 16,
                    Name = "Welcome to C# 7.2 and Span 13",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 18,
                    Name = "Welcome to C# 7.2 and Span 14",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 19,
                    Name = "Welcome to C# 7.2 and Span 15",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 20,
                    Name = "Welcome to C# 7.2 and Span 16",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 21,
                    Name = "Welcome to C# 7.2 and Span 17",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 22,
                    Name = "Welcome to C# 7.2 and Span 18",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 23,
                    Name = "Welcome to C# 7.2 and Span 19",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 24,
                    Name = "Welcome to C# 7.2 and Span 20",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 25,
                    Name = "Welcome to C# 7.2 and Span 21",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 26,
                    Name = "Welcome to C# 7.2 and Span 22",
                    Slug = "welcome-to-c-7-2-and-span",
                    CategoryID = 1,
                    Summary = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "C# is awesome programming language",
                    Content = @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                Enjoy C# 7.2 and Span, and happy hacking!
                                Mads Torgersen, Lead Designer of C#".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                },
                new Post()
                {
                    ID = 27,
                    Name = "Asynchronous vs Deferred JavaScript",
                    Slug = "asynchronous-vs-deffered-javascript",
                    CategoryID = 1,
                    Summary = @"This behaviour can be problematic if we are loading several JavaScript files on a page, as this will interfere with the time to first paint even if the document is not actually dependent on those files.
                                Fortunately, the <script> element has two attributes, async and defer, that can give us more control over how and when external files are fetched and executed",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "khanh.buivuong@nashtechglobal.com",
                    Status = true,
                    Quote = "JavaScript is considered a 'parser blocking resource'. This means that the parsing of the HTML document itself is blocked by JavaScript. When the parser reaches a <script> tag, whether that be internal or external, it stops to fetch (if it is external) and run it",
                    Content = @"Before looking into the effect of the two attributes, we must first look at what occurs in their absence. By default, as mentioned above, JavaScript files will interrupt the parsing of the HTML document in order for them to be fetched (if not inline) and executed.
                                Take, for example, this script element located somewhere in the middle of the page - The HTML parsing is paused for the script to be fetched and executed, thereby extending the amount of time it takes to get to first paint.
                                The async Attribute The async attribute is used to indicate to the browser that the script file can be executed asynchronously. The HTML parser does not need to pause at the point it reaches the script tag to fetch and execute, the execution can happen whenever the script becomes ready after being fetched in parallel with the document parsing.".Trim(),
                    Image = "/Client/images/projects/project-5.png"
                });
        }
        #endregion

        #region Seed Tag Data
        private void SeedTagData(ModelBuilder modelBuilder)
        {

        }
        #endregion

        #region Seed User Data
        public void SeedUserData(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<BloggerUser>()
                .HasData(new BloggerUser()
                {
                    UserName = "khanh.buivuong",
                    //BloggerUserFirstName = "Khanh Vuong",

                });
        }
        #endregion
    }
}
