﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("LikeComments")]
    public class LikeComment
    {
        [Key]
        public Guid LikeCommentId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Guid? CommentId { get; set; }

        [ForeignKey("CommentId")]
        public virtual Comment Comment { get; set; }

        public Guid? ReplyCommentId { get; set; }

        [ForeignKey("ReplyCommentId")]
        public virtual ReplyComment ReplyComment { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }
    }
}
