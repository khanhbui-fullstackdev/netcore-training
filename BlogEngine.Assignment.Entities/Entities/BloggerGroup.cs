﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("BloggerGroups")]
    public class BloggerGroup
    {
        [Key]
        public string BloggerGroupId { get; set; }

        [Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BloggerGroupIdentity { set; get; }

        [StringLength(255), Column(TypeName = "varchar(255)")]
        public string GroupName { set; get; }

        [StringLength(255), Column(TypeName = "nvarchar(255)")]
        public string GroupDescription { set; get; }
    }
}
