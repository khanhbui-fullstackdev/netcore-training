﻿using BlogEngine.Assignment.Common.Constants;
using BlogEngine.Assignment.Entities.BaseEntities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("SubCategories")]
    public class SubCategory : Audit
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [
            Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
            MinLength(2, ErrorMessage = ErrorMessage.MinLengthField),
            MaxLength(255, ErrorMessage = ErrorMessage.MaxLengthField),
            StringLength(255),
            Column(TypeName = "nvarchar(255)")
        ]
        public string Name { get; set; }

        [StringLength(255)]
        [Column(TypeName = "varchar(255)")]
        public string Slug { get; set; }

        [StringLength(500), Column(TypeName = "nvarchar(500)")]
        public string Summary { get; set; }

        public int CategoryID { get; set; }

        [ForeignKey("CategoryID")]
        public virtual Category Category { get; set; }
    }
}
