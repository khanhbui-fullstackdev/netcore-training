﻿using BlogEngine.Assignment.Common.Constants;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("Contacts")]
    public class Contact
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContactId { get; set; }

        [
            Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
            MinLength(2, ErrorMessage = ErrorMessage.MinLengthField),
            MaxLength(255, ErrorMessage = ErrorMessage.MaxLengthField),
            StringLength(255),
            Column(TypeName = "nvarchar(255)")
        ]
        public string ContactName { get; set; }

        [
            EmailAddress(ErrorMessage = ErrorMessage.InvalidEmail),
            Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredEmail),
            StringLength(50), Column(TypeName = "varchar(50)")
        ]
        public string ContactEmail { get; set; }

        [
            Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
            StringLength(500),
            Column(TypeName = "nvarchar(500)")
        ]
        public string Content { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
