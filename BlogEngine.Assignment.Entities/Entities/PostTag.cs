﻿using BlogEngine.Assignment.Entities.BaseEntities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("PostTags")]
    public class PostTag : Audit
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("PostID")]
        [Column(Order = 1)]
        public int PostID { set; get; }

        [ForeignKey("TagID")]
        [Column(Order = 2)]
        public int TagID { set; get; }

        [ForeignKey("PostID")]
        public virtual Post Post { set; get; }

        [ForeignKey("TagID")]
        public virtual Tag Tag { set; get; }
    }
}
