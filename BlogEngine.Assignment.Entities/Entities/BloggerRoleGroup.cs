﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("BloggerRoleGroups")]
    public class BloggerRoleGroup
    {
        [Key]
        public string BloggerRoleGroupId { get; set; }

        [Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BloggerRoleGroupIdentity { get; set; }

        [Column(Order = 1)]
        [ForeignKey("BloggerGroupId")]        
        public string BloggerGroupId { set; get; }

        [Column(Order = 2)]        
        [ForeignKey("BloggerRoleId")]
        public string BloggerRoleId { set; get; }

        [ForeignKey("BloggerRoleId")]
        public virtual BloggerRole BloggerRole { set; get; }

        [ForeignKey("BloggerGroupId")]
        public virtual BloggerGroup BloggerGroup { set; get; }
    }
}
