﻿using BlogEngine.Assignment.Common.Constants;
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("BloggerUsers")]
    public class BloggerUser : IdentityUser<string>
    {  
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
         StringLength(255),
         Column(TypeName = "nvarchar(255)")]
        public string FullName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
         StringLength(255),
         Column(TypeName = "nvarchar(255)")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
         StringLength(255),
         Column(TypeName = "nvarchar(255)")]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
        StringLength(255),
        Column(TypeName = "varchar(255)")]
        public string UserCode { get; set; }

        public DateTime? UserBirthday { get; set; }


        //public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        //{
        //    // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
        //    var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
        //    // Add custom user claims here
        //    return userIdentity;
        //}
    }
}
