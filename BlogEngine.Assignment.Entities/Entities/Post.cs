﻿using BlogEngine.Assignment.Entities.BaseEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("Posts")]
    public class Post : Audit
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(255), Column(TypeName = "nvarchar(255)"), Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [StringLength(255)]
        [Column(TypeName = "varchar(255)")]
        public string Slug { get; set; }

        [Column(TypeName = "varchar(255)"), StringLength(255)]
        public string Image { get; set; }

        [StringLength(500), Column(TypeName = "nvarchar(500)")]
        public string Summary { get; set; }

        public string Content { get; set; }

        public string Quote { get; set; }

        public int CategoryID { get; set; }

        public int CountLikes { get; set; } = 0;

        public int CountComments { get; set; } = 0;

        [ForeignKey("CategoryID")]
        public virtual Category Category { get; set; }

        public virtual IEnumerable<PostTag> PostTags { get; set; }
        public virtual IEnumerable<Comment> Comments { get; set; }
    }
}
