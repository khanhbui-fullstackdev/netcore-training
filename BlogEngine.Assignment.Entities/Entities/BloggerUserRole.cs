﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("BloggerUserRoles")]
    public class BloggerUserRole : IdentityUserRole<string>
    {
        
    }
}
