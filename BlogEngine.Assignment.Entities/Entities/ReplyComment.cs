﻿using BlogEngine.Assignment.Common.Constants;
using BlogEngine.Assignment.Entities.BaseEntities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("ReplyComments")]
    public class ReplyComment : Audit
    {
        [Key]
        public Guid ReplyId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(255), Column(TypeName = "nvarchar(255)")]
        public string Name { get; set; }

        [StringLength(50), Column(TypeName = "varchar(50)")]
        public string UserName { get; set; }

        [EmailAddress(ErrorMessage = ErrorMessage.InvalidEmail), StringLength(50), Column(TypeName = "varchar(50)")]
        public string Email { get; set; }

        public string Content { get; set; }

        public int CountLikes { get; set; } = 0;

        public Guid CommentId { get; set; }

        [ForeignKey("CommentId")]
        public virtual Comment Comment { get; set; }
    }
}
