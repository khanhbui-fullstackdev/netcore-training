﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("BloggerUserGroups")]
    public class BloggerUserGroup
    {
        [Key]
        public string BloggerUserGroupId { get; set; }

        [Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BloggerUserGroupIdentity { get; set; }

        [Column(Order = 1)]
        [ForeignKey("BloggerUserId")]
        public string BloggerUserId { set; get; }

        [Column(Order = 2)]
        [ForeignKey("BloggerGroupId")]
        public string BloggerGroupId { set; get; }

        [ForeignKey("BloggerUserId")]
        public virtual BloggerUser BloggerUser { set; get; }

        [ForeignKey("BloggerGroupId")]
        public virtual BloggerGroup BloggerGroup { set; get; }
    }
}
