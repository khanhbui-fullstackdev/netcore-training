﻿using BlogEngine.Assignment.Common.Constants;
using BlogEngine.Assignment.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("Comments")]
    public class Comment : Audit
    {
        [Key]
        public Guid CommentId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        [StringLength(255), Column(TypeName = "nvarchar(255)")]
        public string Name { get; set; }

        [StringLength(50), Column(TypeName = "varchar(50)")]
        public string UserName { get; set; }

        public string Content { get; set; }

        [EmailAddress(ErrorMessage = ErrorMessage.InvalidEmail), StringLength(50), Column(TypeName = "varchar(50)")]
        public string Email { get; set; }

        public int PostID { get; set; }

        [ForeignKey("PostID")]
        public virtual Post Post { get; set; }

        public int CountReplyComments { get; set; } = 0;

        public int CountLikes { get; set; } = 0;

        public virtual IEnumerable<ReplyComment> ReplyComments { get; set; }
    }
}
