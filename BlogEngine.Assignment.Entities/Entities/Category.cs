﻿using BlogEngine.Assignment.Common.Constants;
using BlogEngine.Assignment.Entities.BaseEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("Categories")]
    public class Category : Audit
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [
            Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredField),
            MinLength(2, ErrorMessage = ErrorMessage.MinLengthField),
            MaxLength(255, ErrorMessage = ErrorMessage.MaxLengthField),
            StringLength(255),
            Column(TypeName = "nvarchar(255)")
        ]
        public string Name { get; set; }

        [StringLength(255)]
        [Column(TypeName = "varchar(255)")]
        public string Slug { get; set; }

        [StringLength(255)]
        [Column(TypeName = "varchar(255)")]
        public string Image { get; set; }

        [StringLength(500), Column(TypeName = "nvarchar(500)")]
        public string Summary { get; set; }

        public virtual IEnumerable<Post> Posts { get; set; }

        public virtual ICollection<SubCategory> SubCategories { get; set; }
    }
}
