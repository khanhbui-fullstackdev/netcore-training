﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.Entities
{
    [Table("BloggerRoles")]
    public class BloggerRole : IdentityRole<string>
    {
        [StringLength(255), Column(TypeName = "nvarchar(255)")]
        public string RoleDescription { set; get; }
    }
}
