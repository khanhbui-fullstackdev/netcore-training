﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BlogEngine.Assignment.Entities.Migrations
{
    public partial class InitiateDatabase_BlogEngineDotNetCore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "BloggerGroups",
                columns: table => new
                {
                    BloggerGroupId = table.Column<string>(nullable: false),
                    BloggerGroupIdentity = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GroupName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    GroupDescription = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerGroups", x => x.BloggerGroupId);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Slug = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Image = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Summary = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    ContactId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContactName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    ContactEmail = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Content = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.ContactId);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Type = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "BloggerRoles",
                schema: "dbo",
                columns: table => new
                {
                    BloggerRoleId = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    RoleName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    NormalizedName = table.Column<string>(maxLength: 255, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    RoleDescription = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerRoles", x => x.BloggerRoleId);
                });

            migrationBuilder.CreateTable(
                name: "BloggerUsers",
                schema: "dbo",
                columns: table => new
                {
                    BloggerUserId = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    UserName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Email = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    UserCode = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    UserBirthday = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerUsers", x => x.BloggerUserId);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Slug = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Image = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Summary = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Quote = table.Column<string>(nullable: true),
                    CategoryID = table.Column<int>(nullable: false),
                    CountLikes = table.Column<int>(nullable: false),
                    CountComments = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Posts_Categories_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "Categories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubCategories",
                columns: table => new
                {
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Slug = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Summary = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    CategoryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategories", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SubCategories_Categories_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "Categories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BloggerRoleGroups",
                columns: table => new
                {
                    BloggerRoleGroupId = table.Column<string>(nullable: false),
                    BloggerRoleGroupIdentity = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BloggerGroupId = table.Column<string>(nullable: true),
                    BloggerRoleId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerRoleGroups", x => x.BloggerRoleGroupId);
                    table.ForeignKey(
                        name: "FK_BloggerRoleGroups_BloggerGroups_BloggerGroupId",
                        column: x => x.BloggerGroupId,
                        principalTable: "BloggerGroups",
                        principalColumn: "BloggerGroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BloggerRoleGroups_BloggerRoles_BloggerRoleId",
                        column: x => x.BloggerRoleId,
                        principalSchema: "dbo",
                        principalTable: "BloggerRoles",
                        principalColumn: "BloggerRoleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BloggerRoleClaims",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    BloggerRoleId = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    ClaimType = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerRoleClaims", x => x.BloggerRoleId);
                    table.UniqueConstraint("AK_BloggerRoleClaims_Id", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BloggerRoleClaims_BloggerRoles_BloggerRoleId",
                        column: x => x.BloggerRoleId,
                        principalSchema: "dbo",
                        principalTable: "BloggerRoles",
                        principalColumn: "BloggerRoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BloggerUserGroups",
                columns: table => new
                {
                    BloggerUserGroupId = table.Column<string>(nullable: false),
                    BloggerUserGroupIdentity = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BloggerUserId = table.Column<string>(nullable: true),
                    BloggerGroupId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerUserGroups", x => x.BloggerUserGroupId);
                    table.ForeignKey(
                        name: "FK_BloggerUserGroups_BloggerGroups_BloggerGroupId",
                        column: x => x.BloggerGroupId,
                        principalTable: "BloggerGroups",
                        principalColumn: "BloggerGroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BloggerUserGroups_BloggerUsers_BloggerUserId",
                        column: x => x.BloggerUserId,
                        principalSchema: "dbo",
                        principalTable: "BloggerUsers",
                        principalColumn: "BloggerUserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BloggerUserClaims",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    BloggerUserId = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerUserClaims", x => x.BloggerUserId);
                    table.UniqueConstraint("AK_BloggerUserClaims_Id", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BloggerUserClaims_BloggerUsers_BloggerUserId",
                        column: x => x.BloggerUserId,
                        principalSchema: "dbo",
                        principalTable: "BloggerUsers",
                        principalColumn: "BloggerUserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BloggerUserLogins",
                schema: "dbo",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    BloggerUserId = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerUserLogins", x => x.BloggerUserId);
                    table.UniqueConstraint("AK_BloggerUserLogins_LoginProvider_ProviderKey", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_BloggerUserLogins_BloggerUsers_BloggerUserId",
                        column: x => x.BloggerUserId,
                        principalSchema: "dbo",
                        principalTable: "BloggerUsers",
                        principalColumn: "BloggerUserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BloggerUserRoles",
                schema: "dbo",
                columns: table => new
                {
                    BloggerUserId = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    BloggerRoleId = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerUserRoles", x => new { x.BloggerUserId, x.BloggerRoleId });
                    table.ForeignKey(
                        name: "FK_BloggerUserRoles_BloggerRoles_BloggerRoleId",
                        column: x => x.BloggerRoleId,
                        principalSchema: "dbo",
                        principalTable: "BloggerRoles",
                        principalColumn: "BloggerRoleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BloggerUserRoles_BloggerUsers_BloggerUserId",
                        column: x => x.BloggerUserId,
                        principalSchema: "dbo",
                        principalTable: "BloggerUsers",
                        principalColumn: "BloggerUserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BloggerUserTokens",
                schema: "dbo",
                columns: table => new
                {
                    BloggerUserId = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 255, nullable: false),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    Value = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloggerUserTokens", x => x.BloggerUserId);
                    table.UniqueConstraint("AK_BloggerUserTokens_BloggerUserId_LoginProvider_Name", x => new { x.BloggerUserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_BloggerUserTokens_BloggerUsers_BloggerUserId",
                        column: x => x.BloggerUserId,
                        principalSchema: "dbo",
                        principalTable: "BloggerUsers",
                        principalColumn: "BloggerUserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    CommentId = table.Column<Guid>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    UserName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Email = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PostID = table.Column<int>(nullable: false),
                    CountReplyComments = table.Column<int>(nullable: false),
                    CountLikes = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.CommentId);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostID",
                        column: x => x.PostID,
                        principalTable: "Posts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LikePosts",
                columns: table => new
                {
                    LikePostId = table.Column<Guid>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 50, nullable: true),
                    PostId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LikePosts", x => x.LikePostId);
                    table.ForeignKey(
                        name: "FK_LikePosts_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostTags",
                columns: table => new
                {
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PostID = table.Column<int>(nullable: false),
                    TagID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostTags_Posts_PostID",
                        column: x => x.PostID,
                        principalTable: "Posts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostTags_Tags_TagID",
                        column: x => x.TagID,
                        principalTable: "Tags",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReplyComments",
                columns: table => new
                {
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    ReplyId = table.Column<Guid>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    UserName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Email = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    CountLikes = table.Column<int>(nullable: false),
                    CommentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReplyComments", x => x.ReplyId);
                    table.ForeignKey(
                        name: "FK_ReplyComments_Comments_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Comments",
                        principalColumn: "CommentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LikeComments",
                columns: table => new
                {
                    LikeCommentId = table.Column<Guid>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommentId = table.Column<Guid>(nullable: true),
                    ReplyCommentId = table.Column<Guid>(nullable: true),
                    UserName = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LikeComments", x => x.LikeCommentId);
                    table.ForeignKey(
                        name: "FK_LikeComments_Comments_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Comments",
                        principalColumn: "CommentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LikeComments_ReplyComments_ReplyCommentId",
                        column: x => x.ReplyCommentId,
                        principalTable: "ReplyComments",
                        principalColumn: "ReplyId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "ID", "CreatedBy", "CreatedDate", "Image", "Name", "Slug", "Status", "Summary", "UpdatedBy" },
                values: new object[,]
                {
                    { 1, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 651, DateTimeKind.Local), "", "C# Programming", "csharp", true, null, null },
                    { 2, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 652, DateTimeKind.Local), "", "ASP.NET Core", "asp.net-core", true, null, null },
                    { 3, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 652, DateTimeKind.Local), "", "ASP.NET Web Application", "asp.net-web-application", true, null, null },
                    { 4, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 652, DateTimeKind.Local), "", "Windows Desktop Application", "windows-desktop-application", true, null, null },
                    { 5, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 652, DateTimeKind.Local), "", "HTML, CSS & Boostrap", "html-css-boostrap", true, null, null },
                    { 6, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 652, DateTimeKind.Local), "", "Javascript Frameworks", "javascript-framework", true, null, null },
                    { 7, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 652, DateTimeKind.Local), "", "Entity FrameWork 6/Entity FrameWork Core", "entity-framework", true, null, null },
                    { 8, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 652, DateTimeKind.Local), "", "Database", "database", true, null, null },
                    { 9, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 652, DateTimeKind.Local), "", "Security", "security", true, null, null }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "ID", "CategoryID", "Content", "CountComments", "CountLikes", "CreatedBy", "CreatedDate", "Image", "Name", "Quote", "Slug", "Status", "Summary", "UpdatedBy" },
                values: new object[,]
                {
                    { 1, 1, @"Today we released a prototype of a C# feature called “nullable reference types“, which is intended to help you find and fix most of your null-related bugs before they blow up at runtime.
                We would love for you to install the prototype and try it out on your code! (Or maybe a copy of it! 😄) Your feedback is going to help us get the feature exactly right before we officially release it.
                The billion-dollar mistake Tony Hoare, one of the absolute giants of computer science and recipient of the Turing Award, invented the null reference! It’s crazy these days to think that something as foundational and ubiquitous was invented, but there it is. Many years later in a talk, Sir Tony actually apologized, calling it his “billion-dollar mistake”:", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Introducing Nullable Reference Types in C#", "Design is not just what is look like, Design is how it's work.", "introducing-nullable-reference-types-in-csharp", true, "Today we released a prototype of a C# feature called “nullable reference types“, which is intended to help you find and fix most of your null-related bugs before they blow up at runtime. We would love for you to install the prototype and try it out on your code! (Or maybe a copy of it!", null },
                    { 3, 6, @"We encountered an issue in the release for jQuery 3.3.0, so we’ve immediately released another tag. The code itself is identical, but our release dependencies (only used during release) were added to the dependencies of the jQuery package itself due to the new behavior of npm in version 5+.
                                                jQuery 3.3.1 is now recommended if installing from npm or GitHub. If using jQuery on any CDN, the built file only differs in the version number.
                                                We apologize for any inconvenience and have updated our release script to account for this issue.
                                                Please see the jQuery 3.3.0 blog post for all relevant code changes in this release.
                                                Download
                                                You can get the files from the jQuery CDN, or link to them directly:

                                                https://code.jquery.com/jquery-3.3.1.js

                                                https://code.jquery.com/jquery-3.3.1.min.js

                                                You can also get this release from npm:

                                                npm install jquery@3.3.1

                                                Slim build
                                                Sometimes you don’t need ajax, or you prefer to use one of the many standalone libraries that focus on ajax requests. And often it is simpler to use a combination of CSS and class manipulation for all your web animations. Along with the regular version of jQuery that includes the ajax and effects modules, we’ve released a “slim” version that excludes these modules. The size of jQuery is very rarely a load performance concern these days, but the slim build is about 6k gzipped bytes smaller than the regular version – 24k vs 30k. These files are also available in the npm package and on the CDN:

                                                https://code.jquery.com/jquery-3.3.1.slim.js
                                                https://code.jquery.com/jquery-3.3.1.slim.min.js

                                                These updates are already available as the current versions on npm and Bower. Information on all the ways to get jQuery is available at https://jquery.com/download/. Public CDNs receive their copies today, please give them a few days to post the files. If you’re anxious to get a quick start, use the files on our CDN until they have a chance to update.

                                                8 THOUGHTS ON “JQUERY 3.3.1 – FIXED DEPENDENCIES IN RELEASE TAG”", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "JQuery 3.3.1 – Fixed Dependencies In Release Tag", "Jquery is always awesome ! Cheer", "jquery-3-3-1–fixed-dependencies-in-release-tag", true, "We encountered an issue in the release for jQuery 3.3.0, so we’ve immediately released another tag. The code itself is identical, but our release dependencies (only used during release) were added to the dependencies of the jQuery package itself due to the new behavior of npm in version 5+.", null },
                    { 2, 2, @"CLI
                                                [4050c6374] The “pack” command under ‘buildCrossTargeting’ for ‘Microsoft.DotNet.MSBuildSdkResolver’ now throws a “NU5104” warning/error because the SDK stage0 was changed to “2.1.300” [change was intended].
                                                [ea539c7f6] Add retry when Directory.Move (#9313)
                                                CoreCLR
                                                [13ea3c2c8e] Fix alternate stack for Alpine docker on SELinux (#17936) (#17975)
                                                [88db627a97] Update g_highest_address and g_lowest_address in StompWriteBarrier(WriteBarrierOp::StompResize) on ARM (#18107)
                                                [0ea5fc4456] Use sysconf(_SC_NPROCESSORS_CONF) instead of sysconf(_SC_NPROCESSORS_ONLN) in PAL and GC on ARM and ARM64
                                                CoreFX
                                                [3700c5b793] Update to a xUnit Performance Api that has a bigger Etw buffer size. … (#30328)
                                                [6b38470265] Use _SC_NPROCESSORS_CONF instead of _SC_NPROCESSORS_ONLN in Unix_ProcessorCountTest on ARM/ARM64 (#30132)
                                                [fe653a068c] check SwitchingProtocol before ContentLength (#29948) (#29993)
                                                [f11f3e1fcf] Fix handling of cursor position when other ESC sequences already in stdin (#29897) (#29923)
                                                [77a4a19622] [release/2.1] Port nano test fixes (#29995)
                                                [7ce9270ac7] Fix Sockets hang caused by concurrent Socket disposal (#29786) (#29846)
                                                [ed23f5391f] Fix terminfo number reading with 32-bit integers (#29655) (#29765)
                                                [1c34018f14] Fix getting attributes for sharing violation files (#29790) (#29832)
                                                [bc71849976] [release/2.1] Fix deadlock when waiting for process exit in Console.CancelKeyPress (#29749)
                                                [adc1c4d0d5] Fix WebSocket split UTF8 read #29834 (#29840) (#29853)
                                                WCF
                                                [0a99dd88] Add net461 as a supported framework for S.SM.Security.
                                                [45855085] Generate ThisAssembly.cs, update the version and links for svcutil.xmlserializer (#2893)
                                                [68457365] Target svcutil.xmlserializer app at dotnetcore. (#2855)", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", ".NET Core 2.1 June Update", "Because of your smile, you make life more beautiful", "net-core-2-1-june-update", true, "We released .NET Core 2.1.1. This update includes .NET Core SDK 2.1.301, ASP.NET Core 2.1.1 and .NET Core 2.1.1. See .NET Core 2.1.1 release notes for complete details on the release. Quality Updates CLI [4050c6374] The “pack” command under ‘buildCrossTargeting’ for ‘Microsoft.DotNet.MSBuildSdkResolver’ now throws a “NU5104” warning/error because the SDK stage0 was changed to “2.1.300” [change was", null },
                    { 27, 1, @"Before looking into the effect of the two attributes, we must first look at what occurs in their absence. By default, as mentioned above, JavaScript files will interrupt the parsing of the HTML document in order for them to be fetched (if not inline) and executed.
                                                Take, for example, this script element located somewhere in the middle of the page - The HTML parsing is paused for the script to be fetched and executed, thereby extending the amount of time it takes to get to first paint.
                                                The async Attribute The async attribute is used to indicate to the browser that the script file can be executed asynchronously. The HTML parser does not need to pause at the point it reaches the script tag to fetch and execute, the execution can happen whenever the script becomes ready after being fetched in parallel with the document parsing.", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Asynchronous vs Deferred JavaScript", "JavaScript is considered a 'parser blocking resource'. This means that the parsing of the HTML document itself is blocked by JavaScript. When the parser reaches a <script> tag, whether that be internal or external, it stops to fetch (if it is external) and run it", "asynchronous-vs-deffered-javascript", true, @"This behaviour can be problematic if we are loading several JavaScript files on a page, as this will interfere with the time to first paint even if the document is not actually dependent on those files.
                                                Fortunately, the <script> element has two attributes, async and defer, that can give us more control over how and when external files are fetched and executed", null },
                    { 26, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 22", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 25, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 21", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 23, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 19", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 22, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 18", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 21, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 17", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 20, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 16", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 19, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 15", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 18, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 14", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 16, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 13", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 24, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 20", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 14, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 11", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 15, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 12", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 5, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 2", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 6, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 3", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 8, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 5", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 7, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 4", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 10, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 7", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 11, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 8", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 12, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 9", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 13, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 10", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 9, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 6", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null },
                    { 4, 1, @"C# 7.2 is the latest point release of C#, and adds a number of small but useful features.
                                                All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that a significant portion of the docs are community contributed, not least the material on the new private protected access modifier.
                                                The dominant theme of C# 7.2 centers on improving expressiveness around working with structs by reference. This is important in particular for high performance scenarios, where structs help avoid the garbage collection overhead of allocation, whereas struct references help avoid excessive copying.
                                                The docs go into detail on this set of features in Reference semantics with value types, and they are shown in my new “talking head” video on Channel 9, New Features in C# 7.1 and C# 7.2.
                                                Several of these features, while generally useful, were added to C# 7.2 specifically in support of the new Span<T> family of framework types. This library offers a unified (and allocation-free) representation of memory from a multitude of different sources, such as arrays, stack allocation and native code. With its slicing capabilities, it obviates the need for expensive copying and allocation in many scenarios, such as string manipulation, buffer management, etc, and provides a safe alternative to unsafe code. This is really a game changer, in my opinion. While you may start out using it mostly for performance-intensive scenarios, it is lightweight enough that I think many new idioms will evolve for using it in every day code.
                                                Jared Parsons gives a great introduction in his Channel 9 video C# 7.2: Understanding Span. In the December 15 issue of the MSDN Magazine, Stephen Toub will go into even more detail on Span and how to use it.
                                                C# 7.2 ships with the 15.5 release of Visual Studio 2017.
                                                Enjoy C# 7.2 and Span, and happy hacking!
                                                Mads Torgersen, Lead Designer of C#", 0, 0, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 659, DateTimeKind.Local), "/Client/images/projects/project-5.png", "Welcome to C# 7.2 and Span 1", "C# is awesome programming language", "welcome-to-c-7-2-and-span", true, "C# 7.2 is the latest point release of C#, and adds a number of small but useful features. All the features are described in wonderful detail in the docs. Start with the overview, What’s new in C# 7.2, which gives you an excellent introduction to the new set of capabilities. It is worth celebrating that", null }
                });

            migrationBuilder.InsertData(
                table: "SubCategories",
                columns: new[] { "ID", "CategoryID", "CreatedBy", "CreatedDate", "Name", "Slug", "Status", "Summary", "UpdatedBy" },
                values: new object[,]
                {
                    { 12, 8, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "My SQL", "mysql", true, null, null },
                    { 7, 6, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "KnockoutJS", "knockoutjs", true, null, null },
                    { 11, 6, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "Vuejs", "vuejs", true, null, null },
                    { 10, 6, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "Reactjs", "Reactjs", true, null, null },
                    { 9, 6, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "Angular 5", "angular5", true, null, null },
                    { 8, 6, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "Angularjs", "angularjs", true, null, null },
                    { 1, 3, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "ASP.NET MVC5", "asp.net-mvc5", true, null, null },
                    { 5, 4, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "Windows Presentation Foundation", "winform-presentation-foundataion", true, null, null },
                    { 4, 4, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "NET Windows Form", "winform", true, null, null },
                    { 3, 3, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "ASP.NET Web Form", "asp.net-web-form", true, null, null },
                    { 2, 2, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "ASP.NET MVC Core", "asp.net-mvc-core", true, null, null },
                    { 15, 1, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "Object Oriented Programming", "oop", true, null, null },
                    { 13, 8, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "Microsoft SQL Server", "microsoft-sql-server", true, null, null },
                    { 6, 6, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "JQuery", "jquery", true, null, null },
                    { 14, 8, "khanh.buivuong@nashtechglobal.com", new DateTime(2018, 11, 12, 19, 10, 41, 654, DateTimeKind.Local), "Oracle", "oracle", true, null, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_BloggerRoleGroups_BloggerGroupId",
                table: "BloggerRoleGroups",
                column: "BloggerGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_BloggerRoleGroups_BloggerRoleId",
                table: "BloggerRoleGroups",
                column: "BloggerRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_BloggerUserGroups_BloggerGroupId",
                table: "BloggerUserGroups",
                column: "BloggerGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_BloggerUserGroups_BloggerUserId",
                table: "BloggerUserGroups",
                column: "BloggerUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostID",
                table: "Comments",
                column: "PostID");

            migrationBuilder.CreateIndex(
                name: "IX_LikeComments_CommentId",
                table: "LikeComments",
                column: "CommentId");

            migrationBuilder.CreateIndex(
                name: "IX_LikeComments_ReplyCommentId",
                table: "LikeComments",
                column: "ReplyCommentId");

            migrationBuilder.CreateIndex(
                name: "IX_LikePosts_PostId",
                table: "LikePosts",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_CategoryID",
                table: "Posts",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_PostTags_PostID",
                table: "PostTags",
                column: "PostID");

            migrationBuilder.CreateIndex(
                name: "IX_PostTags_TagID",
                table: "PostTags",
                column: "TagID");

            migrationBuilder.CreateIndex(
                name: "IX_ReplyComments_CommentId",
                table: "ReplyComments",
                column: "CommentId");

            migrationBuilder.CreateIndex(
                name: "IX_SubCategories_CategoryID",
                table: "SubCategories",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                schema: "dbo",
                table: "BloggerRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BloggerUserRoles_BloggerRoleId",
                schema: "dbo",
                table: "BloggerUserRoles",
                column: "BloggerRoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                schema: "dbo",
                table: "BloggerUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                schema: "dbo",
                table: "BloggerUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BloggerRoleGroups");

            migrationBuilder.DropTable(
                name: "BloggerUserGroups");

            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropTable(
                name: "LikeComments");

            migrationBuilder.DropTable(
                name: "LikePosts");

            migrationBuilder.DropTable(
                name: "PostTags");

            migrationBuilder.DropTable(
                name: "SubCategories");

            migrationBuilder.DropTable(
                name: "BloggerRoleClaims",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "BloggerUserClaims",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "BloggerUserLogins",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "BloggerUserRoles",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "BloggerUserTokens",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "BloggerGroups");

            migrationBuilder.DropTable(
                name: "ReplyComments");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "BloggerRoles",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "BloggerUsers",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
