﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Assignment.Entities.BaseEntities
{
    public abstract class Audit
    {
        public DateTime? CreatedDate { get; set; }

        [StringLength(255), Column(TypeName = "varchar(255)")]
        public string CreatedBy { get; set; }

        [StringLength(255), Column(TypeName = "varchar(255)")]
        public string UpdatedBy { get; set; }

        public bool Status { get; set; }
    }
}
