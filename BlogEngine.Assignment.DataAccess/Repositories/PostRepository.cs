﻿using BlogEngine.Assignment.Common.Enums;
using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.DataAccess.Repositories.IRepositories;
using BlogEngine.Assignment.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.DataAccess.Repositories
{
    public class PostRepository : RepositoryBase<Post>, IPostRepository
    {
        public PostRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public async Task<IEnumerable<Post>> GetPostsBySubCategory(int subcategoryId)
        {
            var posts = from subcategory in BlogEngineDbContext.SubCategories
                        join category in BlogEngineDbContext.Categories on
                        subcategory.CategoryID equals category.ID
                        join post in BlogEngineDbContext.Posts on
                        category.ID equals post.CategoryID
                        where subcategory.ID == subcategoryId && post.Status == true
                        select post;
            return await posts.ToListAsync();
        }

        public IEnumerable<Post> SortPostsByCriteria(IEnumerable<Post> postsEntity, SortingType sortingType)
        {
            switch ((int)sortingType)
            {
                case 1:
                    postsEntity = postsEntity.OrderByDescending(x => x.CountLikes).ToList();
                    break;

                case 2:
                    postsEntity = postsEntity.OrderBy(x => x.CountLikes).ToList();
                    break;

                case 3:
                    postsEntity = postsEntity.OrderByDescending(x => x.CreatedDate.Value).ToList();
                    break;

                case 4:
                    postsEntity = postsEntity.OrderBy(x => x.CreatedDate.Value).ToList();
                    break;

                case 5:
                    postsEntity = postsEntity.OrderByDescending(x => x.CountComments).ToList();
                    break;

                case 6:
                    postsEntity = postsEntity.OrderBy(x => x.CountComments).ToList();
                    break;

                case 7:
                    postsEntity = postsEntity.OrderBy(x => x.Name).ToList();
                    break;

                case 8:
                    postsEntity = postsEntity.OrderByDescending(x => x.Name).ToList();
                    break;

                default:
                    postsEntity = postsEntity.OrderByDescending(x => x.CreatedDate.Value).ToList();
                    break;
            }
            return postsEntity;
        }
    }
}
