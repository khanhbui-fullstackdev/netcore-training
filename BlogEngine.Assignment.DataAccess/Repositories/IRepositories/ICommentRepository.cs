﻿using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.DataAccess.Repositories.IRepositories
{
    public interface ICommentRepository : IRepositoryBase<Comment>
    {
        Task<IEnumerable<Comment>> GetCommentsByPostId(int postId);
    }
}
