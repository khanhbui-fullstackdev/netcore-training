﻿using BlogEngine.Assignment.Common.Enums;
using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.DataAccess.Repositories.IRepositories
{
    public interface IPostRepository : IRepositoryBase<Post>
    {
        Task<IEnumerable<Post>> GetPostsBySubCategory(int subcategoryId);
        IEnumerable<Post> SortPostsByCriteria(IEnumerable<Post> postsEntity, SortingType sortingType);
    }
}
