﻿using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.Entities.Entities;

namespace BlogEngine.Assignment.DataAccess.Repositories.IRepositories
{
    public interface ISubCategoryRepository : IRepositoryBase<SubCategory>
    {

    }
}
