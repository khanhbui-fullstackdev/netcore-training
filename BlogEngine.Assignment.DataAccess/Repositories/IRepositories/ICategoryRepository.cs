﻿using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.DataAccess.Repositories.IRepositories
{
    public interface ICategoryRepository : IRepositoryBase<Category>
    {
        Task<IEnumerable<Category>> GetAllCategoriesWithSubCategories();
    }
}
