﻿using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.DataAccess.Repositories.IRepositories;
using BlogEngine.Assignment.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.DataAccess.Repositories
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public async Task<IEnumerable<Category>> GetAllCategoriesWithSubCategories()
        {
            var categories =  this.BlogEngineDbContext.Categories
               .Include(x => x.SubCategories) // when apply eager loading, Subcategory of navigation property should be change to ICollection
               .Where(x => x.Status);
            return await categories.ToListAsync();
        }
    }
}
