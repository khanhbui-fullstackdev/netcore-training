﻿using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.DataAccess.Repositories.IRepositories;
using BlogEngine.Assignment.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.DataAccess.Repositories
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        public CommentRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public async Task<IEnumerable<Comment>> GetCommentsByPostId(int postId)
        {
            var commentsEntity = BlogEngineDbContext.Comments
                .Where(x => x.PostID == postId && x.Status == true)
                .OrderByDescending(x => x.CreatedDate)
                .Select(x => new Comment
                {
                    CommentId = x.CommentId,
                    Content = x.Content.Trim(),
                    Name = x.Name.Trim(),
                    CreatedDate = x.CreatedDate,
                    CountLikes = x.CountLikes,
                    CountReplyComments = x.CountReplyComments
                });
            return await commentsEntity.ToListAsync();
        }
    }
}
