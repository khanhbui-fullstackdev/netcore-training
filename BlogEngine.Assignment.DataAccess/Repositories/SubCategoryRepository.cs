﻿using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.DataAccess.Repositories.IRepositories;
using BlogEngine.Assignment.Entities.Entities;

namespace BlogEngine.Assignment.DataAccess.Repositories
{
    public class SubCategoryRepository : RepositoryBase<SubCategory>, ISubCategoryRepository
    {
        public SubCategoryRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
