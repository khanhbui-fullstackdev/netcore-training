﻿using BlogEngine.Assignment.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.DataAccess.Infrastrutures
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        #region Properties
        private BlogEngineDbContext _blogEngineDbContext;
        private readonly DbSet<TEntity> _dbSet;

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected BlogEngineDbContext BlogEngineDbContext
        {
            get { return _blogEngineDbContext ?? (_blogEngineDbContext = DbFactory.InitiateBlogEngineDbContext()); }
        }
        #endregion

        #region Constructor
        protected RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            _dbSet = BlogEngineDbContext.Set<TEntity>();
        }
        #endregion

        #region Implementation 
        public virtual TEntity Add(TEntity entity)
        {
            return _blogEngineDbContext.Set<TEntity>().Add(entity).Entity;
        }

        public virtual int Count()
        {
            return _blogEngineDbContext.Set<TEntity>().Count();
        }

        public virtual async Task<int> CountAsync()
        {
            return await _blogEngineDbContext.Set<TEntity>().CountAsync();
        }

        public virtual TEntity Delete(TEntity entity)
        {
            return _blogEngineDbContext.Set<TEntity>().Remove(entity).Entity;
        }


        public virtual TEntity Find(Expression<Func<TEntity, bool>> expression)
        {
            return _blogEngineDbContext.Set<TEntity>().SingleOrDefault(expression);
        }

        public virtual IQueryable<TEntity> FindAll(Expression<Func<TEntity, bool>> expression)
        {
            return _blogEngineDbContext.Set<TEntity>().Where(expression);
        }

        public virtual async Task<ICollection<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match)
        {
            return await _blogEngineDbContext.Set<TEntity>().Where(match).ToListAsync();
        }

        public virtual async Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> match)
        {
            return await _blogEngineDbContext.Set<TEntity>().SingleOrDefaultAsync(match);
        }

        public virtual IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> predicate)
        {
            IQueryable<TEntity> query = _blogEngineDbContext.Set<TEntity>().Where(predicate);
            return query;
        }

        public virtual async Task<ICollection<TEntity>> FindByConditionAsyn(Expression<Func<TEntity, bool>> predicate)
        {
            return await _blogEngineDbContext.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public virtual TEntity GetById(int id)
        {
            return _blogEngineDbContext.Set<TEntity>().Find(id);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return _blogEngineDbContext.Set<TEntity>();
        }

        public virtual async Task<ICollection<TEntity>> GetAllAsyn()
        {
            return await _blogEngineDbContext.Set<TEntity>().ToListAsync();
        }

        public virtual IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> queryable = GetAll();
            foreach (Expression<Func<TEntity, object>> includeProperty in includeProperties)
            {

                queryable = queryable.Include<TEntity, object>(includeProperty);
            }
            return queryable;
        }

        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await _blogEngineDbContext.Set<TEntity>().FindAsync(id);
        }

        public virtual TEntity Update(TEntity entity, object key)
        {
            if (entity == null)
                return null;
            TEntity exist = _blogEngineDbContext.Set<TEntity>().Find(key);
            if (exist != null)
            {
                _blogEngineDbContext.Entry(exist).CurrentValues.SetValues(entity);
                _blogEngineDbContext.SaveChanges();
            }
            return exist;
        }

        public TEntity GetSingleByCondition(Expression<Func<TEntity, bool>> expression, string[] includes = null)
        {
            if (includes != null && includes.Count() > 0)
            {
                var query = _blogEngineDbContext.Set<TEntity>().Include(includes.First());
                foreach (var include in includes.Skip(1))
                    query = query.Include(include);
                return query.FirstOrDefault(expression);
            }
            return _blogEngineDbContext.Set<TEntity>().FirstOrDefault(expression);
        }

        public async Task<TEntity> GetSingleByConditionAsync(Expression<Func<TEntity, bool>> expression, string[] includes = null)
        {
            if (includes != null && includes.Count() > 0)
            {
                var query = _blogEngineDbContext.Set<TEntity>().Include(includes.First());
                foreach (var include in includes.Skip(1))
                    query = query.Include(include);
                return query.FirstOrDefault(expression);
            }
            return await _blogEngineDbContext.Set<TEntity>().FirstOrDefaultAsync(expression);
        }

        public IEnumerable<TEntity> GetMulti(Expression<Func<TEntity, bool>> predicate, string[] includes = null)
        {
            //HANDLE INCLUDES FOR ASSOCIATED OBJECTS IF APPLICABLE
            if (includes != null && includes.Count() > 0)
            {
                var query = _blogEngineDbContext.Set<TEntity>().Include(includes.First());
                foreach (var include in includes.Skip(1))
                    query = query.Include(include);
                return query.Where<TEntity>(predicate).AsQueryable<TEntity>();
            }

            return _blogEngineDbContext.Set<TEntity>().Where<TEntity>(predicate).AsQueryable<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> GetMultiAsync(Expression<Func<TEntity, bool>> predicate, string[] includes = null)
        {
            //HANDLE INCLUDES FOR ASSOCIATED OBJECTS IF APPLICABLE
            if (includes != null && includes.Count() > 0)
            {
                var query = _blogEngineDbContext.Set<TEntity>().Include(includes.First());
                foreach (var include in includes.Skip(1))
                    query = query.Include(include);
                return query.Where<TEntity>(predicate).AsQueryable<TEntity>();
            }

            return await _blogEngineDbContext.Set<TEntity>().Where<TEntity>(predicate).AsQueryable<TEntity>().ToListAsync();
        }

        public virtual TEntity Delete(int id)
        {
            var entity = _dbSet.Find(id);
            return _dbSet.Remove(entity).Entity;
        }

        public virtual void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            _blogEngineDbContext.Entry(entity).State = EntityState.Modified;
        }
        #endregion
    }
}
