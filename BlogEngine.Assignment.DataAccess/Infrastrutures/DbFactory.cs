﻿using BlogEngine.Assignment.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BlogEngine.Assignment.DataAccess.Infrastrutures
{
    public class DbFactory : Disposable, IDbFactory
    {
        private BlogEngineDbContext _blogEngineDbContext;

        public BlogEngineDbContext CreateDbContext(string[] args)
        {
            var builder = DbContextOptionsBuilder();
            return this._blogEngineDbContext ?? (this._blogEngineDbContext = new BlogEngineDbContext(builder.Options));
        }

        public DbContextOptionsBuilder<BlogEngineDbContext> DbContextOptionsBuilder()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().Build();
            var builder = new DbContextOptionsBuilder<BlogEngineDbContext>();
            var connectionString = @"Server=(localhost)\\mssqllocaldb;Database=BlogEngine_Assignment;Trusted_Connection=True;";
            builder.UseSqlServer(connectionString);
            return builder;
        }

        public BlogEngineDbContext InitiateBlogEngineDbContext()
        {
            var builder = DbContextOptionsBuilder();
            return this._blogEngineDbContext ?? (this._blogEngineDbContext = new BlogEngineDbContext(builder.Options));
        }

        protected override void DisposeCore()
        {
            if (_blogEngineDbContext != null)
                _blogEngineDbContext.Dispose();
        }
    }
}
