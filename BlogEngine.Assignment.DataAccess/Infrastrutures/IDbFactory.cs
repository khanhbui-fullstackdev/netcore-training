﻿using BlogEngine.Assignment.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace BlogEngine.Assignment.DataAccess.Infrastrutures
{
    public interface IDbFactory : IDisposable, IDesignTimeDbContextFactory<BlogEngineDbContext>
    {
        BlogEngineDbContext InitiateBlogEngineDbContext();
        DbContextOptionsBuilder<BlogEngineDbContext> DbContextOptionsBuilder();
    }
}
