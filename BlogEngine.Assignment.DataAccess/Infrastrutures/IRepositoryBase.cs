﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.DataAccess.Infrastrutures
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        // Marks an entity as new
        TEntity Add(TEntity entity);

        int Count();

        Task<int> CountAsync();

        // Marks an entity to be removed
        TEntity Delete(TEntity entity);

        TEntity Delete(int id);

        TEntity Find(Expression<Func<TEntity, bool>> expression);

        IQueryable<TEntity> FindAll(Expression<Func<TEntity, bool>> expression);

        Task<ICollection<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> expression);

        Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> expression);

        IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> predicate);

        Task<ICollection<TEntity>> FindByConditionAsyn(Expression<Func<TEntity, bool>> predicate);

        TEntity GetById(int id);

        IQueryable<TEntity> GetAll();

        Task<ICollection<TEntity>> GetAllAsyn();

        IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties);

        Task<TEntity> GetByIdAsync(int id);

        TEntity GetSingleByCondition(Expression<Func<TEntity, bool>> expression, string[] includes = null);

        Task<TEntity> GetSingleByConditionAsync(Expression<Func<TEntity, bool>> expression, string[] includes = null);

        IEnumerable<TEntity> GetMulti(Expression<Func<TEntity, bool>> predicate, string[] includes = null);

        Task<IEnumerable<TEntity>> GetMultiAsync(Expression<Func<TEntity, bool>> predicate, string[] includes = null);

        // Marks an entity as modified
        void Update(TEntity entity);
    }
}
