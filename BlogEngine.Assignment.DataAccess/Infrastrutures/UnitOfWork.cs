﻿using BlogEngine.Assignment.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogEngine.Assignment.DataAccess.Infrastrutures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory dbFactory;
        private BlogEngineDbContext dbContext;

        public UnitOfWork(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }

        public BlogEngineDbContext DbContext
        {
            get { return dbContext ?? (dbContext = dbFactory.InitiateBlogEngineDbContext()); }
        }

        public void Commit()
        {
            
        }
    }
}
