﻿namespace BlogEngine.Assignment.DataAccess.Infrastrutures
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
