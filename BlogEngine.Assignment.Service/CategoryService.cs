﻿using AutoMapper;
using BlogEngine.Assignment.DataAccess.Repositories.IRepositories;
using BlogEngine.Assignment.Dto;
using BlogEngine.Assignment.Service.IServices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public CategoryService(
            ICategoryRepository categoryRepository,
            IMapper mapper)
        {
            this._categoryRepository = categoryRepository;
            this._mapper = mapper;
        }

        public async Task<IEnumerable<CategoryDto>> GetAllCategories()
        {
            var categoriesEntity = await _categoryRepository.GetAllAsyn();
            return _mapper.Map<IEnumerable<CategoryDto>>(categoriesEntity);
        }

        public async Task<IEnumerable<CategoryDto>> GetAllCategoriesWithSubCategories()
        {
            var categoriesEntity = await _categoryRepository.GetAllCategoriesWithSubCategories();
            return _mapper.Map<IEnumerable<CategoryDto>>(categoriesEntity);
        }

        public async Task<IEnumerable<CategoryDto>> GetCategoriesByKeyword(string keyword)
        {
            var categoriesEntity = await _categoryRepository.GetMultiAsync(x => x.Name.Contains(keyword) && x.Status == true);
            return _mapper.Map<IEnumerable<CategoryDto>>(categoriesEntity);
        }

        public async Task<IEnumerable<CategoryDto>> GetCategoriesBySubCategory(int subcategoryId)
        {
            throw new NotImplementedException();
        }

        public async Task<CategoryDto> GetCategoryById(int categoryId)
        {
            var categoryEntity = await _categoryRepository.GetSingleByConditionAsync(x => x.ID == categoryId && x.Status == true);
            return _mapper.Map<CategoryDto>(categoryEntity);
        }
    }
}
