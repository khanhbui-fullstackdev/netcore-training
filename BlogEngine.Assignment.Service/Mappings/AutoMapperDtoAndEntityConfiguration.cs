﻿using AutoMapper;
using BlogEngine.Assignment.Dto;
using BlogEngine.Assignment.Dto.BaseDto;
using BlogEngine.Assignment.Entities.BaseEntities;
using BlogEngine.Assignment.Entities.Entities;

namespace BlogEngine.Assignment.Service.Mappings
{
    public class AutoMapperDtoAndEntityConfiguration : Profile
    {
        public AutoMapperDtoAndEntityConfiguration()
        {
            CreateMap<Audit, AuditDto>();
            CreateMap<Category, CategoryDto>();
            CreateMap<SubCategory, SubCategoryDto>();
            CreateMap<Post, PostDto>();
            CreateMap<Comment, CommentDto>();
            CreateMap<ReplyComment, ReplyCommentDto>();
        }
    }
}
