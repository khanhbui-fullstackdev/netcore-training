﻿using BlogEngine.Assignment.DataAccess.Repositories.IRepositories;
using BlogEngine.Assignment.Entities.Entities;
using BlogEngine.Assignment.Service.IServices;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Service
{
    public class SubCategoryService : ISubCategoryService
    {
        private ISubCategoryRepository _subCategoryRepository;

        public SubCategoryService(ISubCategoryRepository subCategoryRepository)
        {
            this._subCategoryRepository = subCategoryRepository;
        }

        public async Task<SubCategory> GetSubCategoryById(int subcategoryId)
        {
            var subcategory = await _subCategoryRepository.GetSingleByConditionAsync(x => x.ID == subcategoryId && x.Status == true, new string[] { "Category" });
            return subcategory;
        }
    }
}
