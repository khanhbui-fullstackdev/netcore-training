﻿using AutoMapper;
using BlogEngine.Assignment.Common.Enums;
using BlogEngine.Assignment.DataAccess.Infrastrutures;
using BlogEngine.Assignment.DataAccess.Repositories.IRepositories;
using BlogEngine.Assignment.Dto;
using BlogEngine.Assignment.Service.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Service
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PostService(
            IPostRepository postRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this._postRepository = postRepository;
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<IEnumerable<PostDto>> GetAllPosts()
        {
            var posts = await _postRepository.GetAllAsyn();
            return _mapper.Map<IEnumerable<PostDto>>(posts);
        }

        public async Task<Tuple<IEnumerable<PostDto>, int>> GetAllPosts(int page, int pageSize, SortingType sortingType)
        {
            var postsEntity = await _postRepository.GetAllAsyn();
            postsEntity = _postRepository.SortPostsByCriteria(postsEntity, sortingType).ToList();

            var postsDto = postsEntity.Select(p => new PostDto
            {
                ID = p.ID,
                Name = p.Name,
                Slug = p.Slug,
                Image = p.Image,
                CreatedDate = p.CreatedDate,
                CreatedBy = p.CreatedBy,
                CountComments = p.CountComments,
                Summary = p.Summary
            });
            postsDto = postsDto.Skip((page - 1) * pageSize).Take(pageSize);

            Tuple<IEnumerable<PostDto>, int> posts =
                        new Tuple<IEnumerable<PostDto>, int>(postsDto, postsEntity.Count());
            return posts;
        }

        public async Task<PostDto> GetPostById(int postId)
        {
            var postEntity = await _postRepository.GetSingleByConditionAsync(x => x.ID == postId && x.Status == true);
            var postDto = _mapper.Map<PostDto>(postEntity);
            return postDto;
        }

        public async Task<Tuple<IEnumerable<PostDto>, int>> GetPostsByCategory(int categoryId, int page, int pageSize, SortingType sortingType)
        {
            var postsEntity = await _postRepository
                .GetMultiAsync(x => x.CategoryID == categoryId && x.Status == true);
            postsEntity = _postRepository.SortPostsByCriteria(postsEntity, sortingType).ToList();

            var postsDto = postsEntity.Select(p => new PostDto
            {
                ID = p.ID,
                Name = p.Name,
                Slug = p.Slug,
                Image = p.Image,
                CreatedDate = p.CreatedDate,
                CreatedBy = p.CreatedBy,
                CountComments = p.CountComments,
                Summary = p.Summary
            });
            postsDto = postsDto.Skip((page - 1) * pageSize).Take(pageSize);

            Tuple<IEnumerable<PostDto>, int> posts =
                        new Tuple<IEnumerable<PostDto>, int>(postsDto, postsEntity.Count());
            return posts;
        }

        public async Task<Tuple<IEnumerable<PostDto>, int>> GetPostsByCategory(string slug, int page, int pageSize, SortingType sortingType)
        {
            var postsEntity = await _postRepository.GetMultiAsync(x => x.Slug.Equals(slug) && x.Status == true);
            postsEntity = _postRepository.SortPostsByCriteria(postsEntity, sortingType);

            var postsDto = postsEntity.Select(p => new PostDto
            {
                ID = p.ID,
                Name = p.Name,
                Slug = p.Slug,
                Image = p.Image,
                CreatedDate = p.CreatedDate,
                CreatedBy = p.CreatedBy,
                CountComments = p.CountComments,
                Summary = p.Summary
            });
            postsDto = postsDto.Skip((page - 1) * pageSize).Take(pageSize);

            Tuple<IEnumerable<PostDto>, int> posts =
                        new Tuple<IEnumerable<PostDto>, int>(postsDto, postsEntity.Count());
            return posts;
        }

        public async Task<Tuple<IEnumerable<PostDto>, int>> GetPostsByKeyword(string keyword, int page, int pageSize, SortingType sortingType)
        {
            var postsEntity = await _postRepository.GetMultiAsync(
                x => x.Name.Contains(keyword) &&
                x.Status == true,
                new string[] { "Category" });
            postsEntity = _postRepository.SortPostsByCriteria(postsEntity, sortingType);

            var postsDto = postsEntity.Select(p => new PostDto
            {
                ID = p.ID,
                Name = p.Name,
                Slug = p.Slug,
                Image = p.Image,
                CreatedDate = p.CreatedDate,
                CreatedBy = p.CreatedBy,
                CountComments = p.CountComments,
                Summary = p.Summary
            });
            postsDto = postsDto.Skip((page - 1) * pageSize).Take(pageSize);

            Tuple<IEnumerable<PostDto>, int> posts =
                        new Tuple<IEnumerable<PostDto>, int>(postsDto, postsEntity.Count());
            return posts;
        }

        public async Task<IEnumerable<PostDto>> GetPostsByKeyword(string keyword)
        {
            var postsEntity = await _postRepository.GetMultiAsync(
                 x => x.Name.Contains(keyword) &&
                 x.Status == true,
                 new string[] { "Category" });

            var postsDto = postsEntity.Select(p => new PostDto
            {
                ID = p.ID,
                Name = p.Name,
                Slug = p.Slug,
                Image = p.Image,
                CreatedDate = p.CreatedDate,
                CreatedBy = p.CreatedBy,
                CountComments = p.CountComments,
                Summary = p.Summary
            });
            return postsDto;
        }

        public async Task<Tuple<IEnumerable<PostDto>, int>> GetPostsBySubcategory(int subcategoryId, int page, int pageSize, SortingType sortingType)
        {
            var postsEntity = await _postRepository.GetPostsBySubCategory(subcategoryId);
            postsEntity = _postRepository.SortPostsByCriteria(postsEntity, sortingType);

            var postsDto = postsEntity.Select(p => new PostDto
            {
                ID = p.ID,
                Name = p.Name,
                Slug = p.Slug,
                Image = p.Image,
                CreatedDate = p.CreatedDate,
                CreatedBy = p.CreatedBy,
                CountComments = p.CountComments,
                Summary = p.Summary
            });
            postsDto = postsDto.Skip((page - 1) * pageSize).Take(pageSize);

            Tuple<IEnumerable<PostDto>, int> posts =
                        new Tuple<IEnumerable<PostDto>, int>(postsDto, postsEntity.Count());
            return posts;
        }

        public Task UpdatePost(PostDto post)
        {
            throw new NotImplementedException();
        }
    }
}
