﻿using BlogEngine.Assignment.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Service.IServices
{
    public interface ICommentService
    {
        Task<CommentDto> AddNewComment(CommentDto comment);
        Task<IEnumerable<CommentDto>> GetCommentsByPostId(int postId);
        Task<CommentDto> GetCommentById(int commentId);
        Task<CommentDto> GetCommentById(Guid commentId);
        Task UpdateComment(CommentDto comment);
        Task SaveChangesAsync();
    }
}
