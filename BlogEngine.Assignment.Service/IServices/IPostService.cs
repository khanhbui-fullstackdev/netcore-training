﻿using BlogEngine.Assignment.Common.Enums;
using BlogEngine.Assignment.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Service.IServices
{
    public interface IPostService
    {
        Task<IEnumerable<PostDto>> GetAllPosts();
        Task<Tuple<IEnumerable<PostDto>, int>> GetAllPosts(int page, int pageSize, SortingType sortingType);
        Task<PostDto> GetPostById(int postId);
        Task<Tuple<IEnumerable<PostDto>, int>> GetPostsByCategory(int categoryId, int page, int pageSize, SortingType sortingType);
        Task<Tuple<IEnumerable<PostDto>, int>> GetPostsByCategory(string slug, int page, int pageSize, SortingType sortingType);
        Task<Tuple<IEnumerable<PostDto>, int>> GetPostsBySubcategory(int subcategoryId, int page, int pageSize, SortingType sortingType);
        Task<Tuple<IEnumerable<PostDto>, int>> GetPostsByKeyword(string keyword, int page, int pageSize, SortingType sortingType);
        Task<IEnumerable<PostDto>> GetPostsByKeyword(string keyword);
        Task UpdatePost(PostDto post);
    }
}
