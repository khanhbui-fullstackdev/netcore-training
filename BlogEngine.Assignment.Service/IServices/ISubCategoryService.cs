﻿using BlogEngine.Assignment.Entities.Entities;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Service.IServices
{
    public interface ISubCategoryService
    {
        Task<SubCategory> GetSubCategoryById(int subcategoryId);
    }
}
