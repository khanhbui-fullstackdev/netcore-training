﻿using BlogEngine.Assignment.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Service.IServices
{
    public interface ICategoryService
    {
        Task<CategoryDto> GetCategoryById(int categoryId);
        Task<IEnumerable<CategoryDto>> GetAllCategories();
        Task<IEnumerable<CategoryDto>> GetAllCategoriesWithSubCategories();
        Task<IEnumerable<CategoryDto>> GetCategoriesBySubCategory(int subcategoryId);
        Task<IEnumerable<CategoryDto>> GetCategoriesByKeyword(string keyword);
    }
}

