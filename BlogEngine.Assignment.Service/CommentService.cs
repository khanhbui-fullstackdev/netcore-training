﻿using AutoMapper;
using BlogEngine.Assignment.DataAccess.Repositories.IRepositories;
using BlogEngine.Assignment.Dto;
using BlogEngine.Assignment.Service.IServices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.Assignment.Service
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IMapper _mapper;
        public CommentService(
            ICommentRepository commentRepository,
            IMapper mapper)
        {
            this._commentRepository = commentRepository;
            this._mapper = mapper;
        }

        public Task<CommentDto> AddNewComment(CommentDto comment)
        {
            throw new NotImplementedException();
        }

        public Task<CommentDto> GetCommentById(int commentId)
        {
            throw new NotImplementedException();
        }

        public Task<CommentDto> GetCommentById(Guid commentId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CommentDto>> GetCommentsByPostId(int postId)
        {
            var commentsEntity = await _commentRepository.GetCommentsByPostId(postId);
            var commentsDto = _mapper.Map<IEnumerable<CommentDto>>(commentsEntity);
            return commentsDto;
        }

        public Task SaveChangesAsync()
        {
            throw new NotImplementedException();
        }

        public Task UpdateComment(CommentDto comment)
        {
            throw new NotImplementedException();
        }
    }
}
